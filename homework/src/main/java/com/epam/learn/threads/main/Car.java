package com.epam.learn.threads.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Car extends Thread {
    private final int id;
    private final List<Parking> parkingPlaces;

    public Car(int id, List<Parking> parkingPlaces) {
        this.id = id;
        this.parkingPlaces = new ArrayList<>(parkingPlaces);
    }

    @Override
    public void run() {
        while (!parkingPlaces.isEmpty()) {
            Parking parkingPlace = chooseParking();
                if (parkingPlace.takeParkingPlace(2)) {
                    try {
                        System.out.printf("Car #%s is TAKING parking place in %s... \n", this.id, parkingPlace.getAddress());
                        stayParked();
                        leaveParking();
                    }  catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        parkingPlace.releaseParkingPlace();
                    }
                    break;
                } else if (parkingPlaces.isEmpty()) {
                    goAway();
                } else {
                    goToAnotherParking();
                }
        }
    }

    private void leaveParking() {
        System.out.printf("Car #%s is LEAVING its parking place... \n \n", this.id);
    }

    private void goToAnotherParking() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Car #%s is going to another parking... \n \n", this.id);
    }

    private void stayParked() throws InterruptedException {
        int timeout = new Random().nextInt(10);
        if(timeout == 0) {
            timeout++;
        }
        TimeUnit.SECONDS.sleep(timeout);
        System.out.printf("Car #%s has been staying parked for %d second(s). \n \n", this.id, timeout);
    }

    private void goAway() {
        System.out.printf("Car #%s didn't succeed to find parking place and LEAVES. \n \n", this.id);
    }

    private Parking chooseParking() {
        Parking chosenParking = parkingPlaces.get(new Random().nextInt(parkingPlaces.size()));
        parkingPlaces.remove(chosenParking);
        return chosenParking;
    }
}