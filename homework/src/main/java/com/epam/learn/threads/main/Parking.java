package com.epam.learn.threads.main;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Parking extends Semaphore{
    private final String address;

    public Parking(int permits, String address) {
        super(permits);
        this.address = address;
    }
    public String getAddress() {
        return address;
    }

    public boolean takeParkingPlace(int timeout) {
        try {
            return this.tryAcquire(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void releaseParkingPlace() {
        this.release();
    }
}