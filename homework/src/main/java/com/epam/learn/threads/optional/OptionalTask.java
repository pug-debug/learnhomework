package com.epam.learn.threads.optional;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class OptionalTask {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for(int i = 0; i < 10; i++) {
            executorService.submit(new PlaneWork(i));
        }
        executorService.shutdown();
    }
}
class PlaneWork implements Runnable {
    private final int id;

    public PlaneWork(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        try {
            System.out.printf("Plane %d began to enter the runway... \n", id);
            TimeUnit.SECONDS.sleep(1);
            System.out.printf("Runway accepted plane %d. \n", id);
            TimeUnit.SECONDS.sleep(1);
            System.out.printf("Plane %d took off. \n", id);
            TimeUnit.SECONDS.sleep(1);
            System.out.println("Runway is free.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}