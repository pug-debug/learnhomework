package com.epam.learn.threads.main;

import java.util.Arrays;
import java.util.List;

public class TestClass {
    public static void main(String[] args) {
        List<Parking> parkings = Arrays.asList(
                new Parking(1, "Napoleona Ordy str. 2"),
                new Parking(2, "Nemiga str. 8"));

        List<Car> carList = Arrays.asList(
                new Car(1, parkings),
                new Car(2, parkings),
                new Car(3, parkings),
                new Car(4, parkings),
                new Car(5, parkings),
                new Car(6, parkings),
                new Car(7, parkings),
                new Car(8, parkings));

        for(Car car : carList) {
            car.start();
        }
    }
}