package com.epam.learn.fundamentals.optional.task1;

import java.util.ArrayList;
import java.util.Scanner;


public class OptionalTask1_4 {
    public static void main(String[] args) {
        int[] inputArray = getNumberArray();

        int minRepeats = 0;
        int indexOfNumberWithMinRepeats = 0;
        int maxRepeats = 0;
        int indexOfNumberWithMaxRepeats = 0;

        for(int i = 0; i < inputArray.length; i++) {
            int repeatsForCurrentNumber = countAllDigitRepeatsInNumber(inputArray[i]);
            if (repeatsForCurrentNumber > maxRepeats) {
                maxRepeats = repeatsForCurrentNumber;
                indexOfNumberWithMaxRepeats = i;
            }
            if (repeatsForCurrentNumber < minRepeats) {
                minRepeats = repeatsForCurrentNumber;
                indexOfNumberWithMinRepeats = i;
            }
        }
        System.out.println("Number with minimal count of repeats is "
                + inputArray[indexOfNumberWithMinRepeats] + ". Count of repeats is " + minRepeats);
        System.out.println("Number with maximal count of repeats is "
                + inputArray[indexOfNumberWithMaxRepeats] + ". Count of repeats is " + maxRepeats);
    }

    public static int[] getNumberArray() {
        Scanner inputNumbers = new Scanner(System.in);
        ArrayList<Integer> listOfInputNumbers = new ArrayList<>();
        while (inputNumbers.hasNext()) {
            listOfInputNumbers.add(inputNumbers.nextInt());
        }
        return listOfInputNumbers.stream().mapToInt(i -> i).toArray();
    }

    public static int countAllDigitRepeatsInNumber(int number){
        ArrayList<Integer> digits = getDigitsInNumber(number);
        int digitRepeats = 0;
        for(Integer digit : digits) {
            int repeats = getCountOfRepeatsOfCurrentDigit(number, digit);
            digitRepeats += repeats;
        }
        return digitRepeats;
    }

    public static int getCountOfRepeatsOfCurrentDigit(int number, int digit) {
        return Integer.toString(number).length() - Integer.toString(number)
                .replaceAll(Integer.toString(digit),"").length();
    }

    public static ArrayList<Integer> getDigitsInNumber(int inputNumber) {
        ArrayList<Integer> listOfDigits = new ArrayList<>();
        String numberAsString = Integer.toString(Math.abs(inputNumber));
        for(int i = 0; i < numberAsString.length(); i++) {
            String currentDigit = numberAsString.substring(i, i+1);
            Integer currentDigitAsInteger = Integer.parseInt(currentDigit);
            if(!listOfDigits.contains(currentDigitAsInteger)) {
                listOfDigits.add(currentDigitAsInteger);
            }
        }
        return listOfDigits;
    }
}