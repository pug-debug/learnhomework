package com.epam.learn.fundamentals.main;

import java.util.Scanner;

public class MainTask5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Hello! Enter a number from 1 to 12: ");
        int inputNumber = scanner.nextInt();
        System.out.println(getMonthByNumber(inputNumber));

    }
    public static String getMonthByNumber(int numberOfMonth) {
        String monthName;
        switch (numberOfMonth) {
            case 1:
                monthName = "January";
                break;
            case 2:
                monthName = "February";
                break;
            case 3:
                monthName = "March";
                break;
            case 4:
                monthName = "April";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "June";
                break;
            case 7:
                monthName = "July";
                break;
            case 8:
                monthName = "August";
                break;
            case 9:
                monthName = "September";
                break;
            case 10:
                monthName = "October";
                break;
            case 11:
                monthName = "November";
                break;
            case 12:
                monthName = "December";
                break;
            default:
                monthName = "Sorry. You entered wrong number. It should be between 1 and 12.";
                break;
        }
        return monthName;
    }
}
