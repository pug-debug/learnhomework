package com.epam.learn.fundamentals.optional.task1;

import java.util.ArrayList;
import java.util.Scanner;


public class OptionalTask1_5 {
    public static void main(String[] args) {
        int[] inputArray = getNumberArray();
        int countOfEvenDigits = 0;
        int countOfNonEven = 0;
        for (int j : inputArray) {
            if (areAllDigitsEven(j)) {
                countOfEvenDigits += 1;
            } else if (isNumberOfEvenDigitsEqualOddDigits(j)) {
                countOfNonEven += 1;
            }
        }
        System.out.println("Amount of numbers with all even digits is " + countOfEvenDigits + ".");
        System.out.println("Amount of numbers with equal number of even and non-even digits is " + countOfNonEven + ".");
    }

    public static int[] getNumberArray() {
        Scanner inputNumbers = new Scanner(System.in);
        ArrayList<Integer> listOfInputNumbers = new ArrayList<>();
        while (inputNumbers.hasNext()) {
            listOfInputNumbers.add(inputNumbers.nextInt());
        }
        return listOfInputNumbers.stream().mapToInt(i -> i).toArray();
    }

    public static boolean areAllDigitsEven(int number) {
        boolean areAllDigitsEven = true;
        ArrayList<Integer> numberDigits = getDigitsInNumber(number);
        for (Integer digit : numberDigits) {
            if (!isEven(digit)) {
                areAllDigitsEven = false;
                break;
            }
        }
        return areAllDigitsEven;
    }

    public static ArrayList<Integer> getDigitsInNumber(int number) {
        ArrayList<Integer> listOfDigits = new ArrayList<>();
        String numberAsString = Integer.toString(Math.abs(number));
        for (int i = 0; i < numberAsString.length(); i++) {
            String currentDigit = numberAsString.substring(i, i + 1);
            Integer currentDigitAsInteger = Integer.parseInt(currentDigit);
            listOfDigits.add(currentDigitAsInteger);
        }
        return listOfDigits;
    }

    public static boolean isEven(int digit) {
        boolean isEven = false;
        if (digit % 2 == 0) {
            isEven = true;
        }
        return isEven;
    }

    public static boolean isNumberOfEvenDigitsEqualOddDigits(int number) {
        boolean isEvenEqualNonEven = false;
        ArrayList<Integer> listOfDigits = getDigitsInNumber(number);
        int countOfEvenDigits = 0;
        int countOfOddDigits = 0;
        for (Integer listOfDigit : listOfDigits) {
            if (isEven(listOfDigit)) {
                countOfEvenDigits++;
            } else {
                countOfOddDigits++;
            }
        }
        if(countOfEvenDigits == countOfOddDigits) {
            isEvenEqualNonEven = true;
        }
        return isEvenEqualNonEven;
    }
}
