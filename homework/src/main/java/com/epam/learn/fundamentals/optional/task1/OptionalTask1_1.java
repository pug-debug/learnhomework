package com.epam.learn.fundamentals.optional.task1;

import java.util.ArrayList;
import java.util.Scanner;

public class OptionalTask1_1 {
    public static void main(String[] args) {
        int[] numberArray = getNumberArray();

        int minIndex = 0;
        int minLength = numberLength(numberArray[minIndex]);
        int maxIndex = 0;
        int maxLength = numberLength(numberArray[maxIndex]);

        for(int k = 1; k < numberArray.length; k++) {
            int currentLength = numberLength(numberArray[k]);
            if (currentLength < minLength) {
                minIndex = k;
                minLength = currentLength;
            } else if (currentLength > maxLength) {
                maxIndex = k;
                maxLength = currentLength;
            }
        }
        System.out.println("The shortest number is " + numberArray[minIndex]);
        System.out.println("The longest number is " + numberArray[maxIndex]);
    }

    public static int[] getNumberArray() {
        Scanner inputNumbers = new Scanner(System.in);
        ArrayList<Integer> listOfInputNumbers = new ArrayList<>();
        while (inputNumbers.hasNext()) {
            String line = inputNumbers.nextLine();
            if (line.equals("exit") || line.equals("")) {
                break;
            }
            int var = Integer.parseInt(line);
            System.out.println("your number: " + var);
            listOfInputNumbers.add(var);
        }
        return listOfInputNumbers.stream().mapToInt(i -> i).toArray();
    }

    public static int numberLength(int number){
        String numberString = Integer.toString(Math.abs(number));
        return numberString.length();
    }
}
