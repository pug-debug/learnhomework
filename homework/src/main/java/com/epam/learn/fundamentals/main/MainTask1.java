package com.epam.learn.fundamentals.main;

import java.util.Scanner;

public class MainTask1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Hi! Enter your name: ");

        String inputName = scanner.nextLine();
        System.out.printf("Hello, %s!", inputName);
    }
}
