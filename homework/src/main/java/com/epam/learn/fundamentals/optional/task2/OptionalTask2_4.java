package com.epam.learn.fundamentals.optional.task2;

import java.util.Scanner;

public class OptionalTask2_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter matrix size: ");
        int numberOfRowsAndColumns = scanner.nextInt();

        int[][] matrix = new int[numberOfRowsAndColumns][numberOfRowsAndColumns];

        generateMatrix(matrix);
        printMatrix(matrix);
        System.out.println("===================================");

        int[] maxValueCoordinates = getMaxMatrixElementInfo(matrix);
        int rowWithMaxElement = maxValueCoordinates[0];
        int columnWithMaxElement = maxValueCoordinates[1];

        int[][] newMatrix = deleteRowAndColumn(matrix, rowWithMaxElement, columnWithMaxElement);
        printMatrix(newMatrix);
    }

    public static void generateMatrix(int[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) ((Math.random()*200)-100);
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                System.out.printf("%4d", matrix[row][column]);
            }
            System.out.println();
        }
    }

    public static int[] getMaxMatrixElementInfo(int[][] matrix) {
        int rowWithMaxElement = 0;
        int columnWithMaxElement = 0;
        int maxValue = matrix[rowWithMaxElement][columnWithMaxElement];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] > maxValue) {
                    maxValue = matrix[i][j];
                    rowWithMaxElement = i;
                    columnWithMaxElement = j;
                }
            }
        }
        System.out.println("Max value: " + maxValue);
        System.out.println("Column with max value: " + (columnWithMaxElement+1));
        System.out.println("Row with max value: " + (rowWithMaxElement+1));
        return new int[] {rowWithMaxElement, columnWithMaxElement};
    }

    public static int[][] deleteRowAndColumn(int[][] matrix, int indexOfRow, int indexOfColumn) {
        int numberOfColumns = matrix[0].length;
        int numberOfRows = matrix.length;
        int[][] matrixWithoutMaxElement = new int[numberOfRows - 1][numberOfColumns - 1];

        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                if (i < indexOfRow && j < indexOfColumn) {
                    matrixWithoutMaxElement[i][j] = matrix[i][j];
                }
                if (i > indexOfRow && j > indexOfColumn) {
                    matrixWithoutMaxElement[i - 1][j - 1] = matrix[i][j];
                }
                if (i < indexOfRow && j > indexOfColumn) {
                    matrixWithoutMaxElement[i][j - 1] = matrix[i][j];
                }
                if (i > indexOfRow && j < indexOfColumn) {
                    matrixWithoutMaxElement[i - 1][j] = matrix[i][j];
                }
            }
        }
        return matrixWithoutMaxElement;
    }
}

