package com.epam.learn.fundamentals.optional.task2;

import java.util.Arrays;
import java.util.Scanner;

public class OptionalTask2_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter matrix size: ");
        int numberOfRowsAndColumns = scanner.nextInt();

        int[][] matrix = new int[numberOfRowsAndColumns][numberOfRowsAndColumns];

        generateMatrix(matrix);
        printMatrix(matrix);

        int[][] copyMatrix = Arrays.stream(matrix).map(int[]::clone).toArray(int[][]::new);

        System.out.println("Enter k column for sorting rows by column: ");
        int columnNumber = scanner.nextInt();

        System.out.println("Enter p rows for sorting columns by row: ");
        int rowNumber = scanner.nextInt();

        System.out.println("=====================");

        bubbleSortByRows(matrix, columnNumber);
        printMatrix(matrix);
        System.out.println("=====================");

        bubbleSortByColumns(copyMatrix, rowNumber);
        printMatrix(copyMatrix);
    }

    public static void generateMatrix(int[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) ((Math.random()*20)-10);
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                System.out.printf("%4d", matrix[row][column]);
            }
            System.out.println();
        }
    }

    public static void bubbleSortByColumns(int[][] matrix, int row) {
        int numberOfColumns = matrix[0].length;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for(int j = 0; j < numberOfColumns-1; j++) {
                if(matrix[row][j] > matrix[row][j+1]) {
                    isSorted = false;
                    changeColumns(matrix, j, j+1);
                }
            }
        }
    }

    public static void changeColumns(int[][] matrix, int column1, int column2) {
        int numberOfRows = matrix.length;
        for (int i = 0; i < numberOfRows; i++) {
            int buf = matrix[i][column1];
            matrix[i][column1] = matrix[i][column2];
            matrix[i][column2] = buf;
        }
    }

    public static void changeRows(int[][] matrix, int indexOfFirstRow, int indexOfSecondRow) {
        int numberOfColumns = matrix[0].length;
        for (int k = 0; k < numberOfColumns; k++) {
            int buf = matrix[indexOfFirstRow][k];
            matrix[indexOfFirstRow][k] = matrix[indexOfSecondRow][k];
            matrix[indexOfSecondRow][k] = buf;
        }
    }

    public static void bubbleSortByRows(int[][] matrix, int indexOfColumn) {
        int numberOfRows = matrix.length;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int j = 0; j < numberOfRows - 1; j++) {
                if (matrix[j][indexOfColumn] > matrix[j + 1][indexOfColumn]) {
                    isSorted = false;
                    changeRows(matrix, j, j + 1);
                }
            }
        }
    }
}
