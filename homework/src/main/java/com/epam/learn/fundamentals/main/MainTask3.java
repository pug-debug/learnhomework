package com.epam.learn.fundamentals.main;

import java.util.Scanner;

public class MainTask3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Hello! Enter some number: ");
        int inputNumber = scanner.nextInt();

        getListOfNumbers(inputNumber);
        getListOfNumbersInOneLine(inputNumber);
    }

    public static void getListOfNumbers(int numOfNumbers) {
        for (int i = 0; i < numOfNumbers; i++) {
            int number = (int) (Math.random() * 100);
            System.out.println(number);
        }
    }

    public static void getListOfNumbersInOneLine(int numOfNumbers) {
        for (int i = 0; i < numOfNumbers; i++) {
            int number = (int) (Math.random() * 100);
            System.out.print(number + " ");
        }
    }
}
