package com.epam.learn.fundamentals.main;

public class MainTask4 {
    public static void main(String[] args) {
        int sumOfArguments = 0;
        int productOfArguments = 1;
        for(String arg: args) {
            sumOfArguments += Integer.parseInt(arg);
            productOfArguments *= Integer.parseInt(arg);
        }
        System.out.printf("Sum of arguments is %s \n", sumOfArguments);
        System.out.printf("Product of arguments is %s \n", productOfArguments);
    }
}
