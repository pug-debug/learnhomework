package com.epam.learn.fundamentals.optional.task1;

import java.util.*;

public class OptionalTask1_2 {
    public static void main(String[] args) {
        int[] inputArray = getNumberArray();
        sortArray(inputArray);
        System.out.println(Arrays.toString(inputArray));
        invertArray(inputArray);
        System.out.println(Arrays.toString(inputArray));
    }

    public static int[] getNumberArray() {
        Scanner inputNumbers = new Scanner(System.in);
        ArrayList<Integer> listOfInputNumbers = new ArrayList<>();
        while (inputNumbers.hasNext()) {
            listOfInputNumbers.add(inputNumbers.nextInt());
        }
        return listOfInputNumbers.stream().mapToInt(i -> i).toArray();
    }

    public static void sortArray(int[] numbers) {
        boolean isSorted = false;
        int buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < numbers.length - 1; i++) {
                if (isFirstNumberShorterThanSecond(numbers[i], numbers[i+1])) {
                    isSorted = false;

                    buf = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = buf;
                }
            }
        }
    }

    public static boolean isFirstNumberShorterThanSecond(int firstNumber, int secondNumber) {
        boolean isShorter;
        if(getNumberLength(firstNumber) < getNumberLength(secondNumber)) {
            isShorter = true;
        }
        else if (getNumberLength(firstNumber) == getNumberLength(secondNumber)) {
            isShorter = firstNumber < secondNumber;
        }
        else isShorter = false;
        return isShorter;
    }

    public static int getNumberLength(int number) {
        String numberString = Integer.toString(Math.abs(number));
        return numberString.length();
    }

    public static void invertArray(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
    }
}
