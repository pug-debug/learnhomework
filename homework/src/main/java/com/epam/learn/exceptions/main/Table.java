package com.epam.learn.exceptions.main;

import com.epam.learn.exceptions.main.exceptions.InvalidMarkException;
import com.epam.learn.exceptions.main.exceptions.NoSubjectsForStudentException;
import com.epam.learn.exceptions.main.universityEntities.Subject;

import java.util.List;
import java.util.Map;

public interface Table {
    void gradeStudentInSubject(Student student, Subject subject, Integer mark) throws InvalidMarkException;
    Map<Subject, Integer> getMarksForStudent(Student student) throws NoSubjectsForStudentException;
    Integer getStudentMarkBySubject(Student student, Subject subject);
    List<Integer> getAllMarksBySubject(Subject subject);
}
