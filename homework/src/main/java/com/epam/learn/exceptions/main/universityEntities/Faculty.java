package com.epam.learn.exceptions.main.universityEntities;

public enum Faculty {
    MMF, GEO, BIO, FEN
}
