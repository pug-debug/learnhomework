package com.epam.learn.exceptions.main.exceptions;

public class InvalidMarkException extends Exception{
    public InvalidMarkException(String message) {
        super(message);
    }
}
