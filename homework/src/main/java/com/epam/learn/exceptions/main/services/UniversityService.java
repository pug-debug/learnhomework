package com.epam.learn.exceptions.main.services;

import com.epam.learn.exceptions.main.MarkTable;
import com.epam.learn.exceptions.main.Student;
import com.epam.learn.exceptions.main.exceptions.*;
import com.epam.learn.exceptions.main.universityEntities.Faculty;
import com.epam.learn.exceptions.main.universityEntities.Subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UniversityService {
    private final Map<Faculty, List<Integer>> mapOfFacultiesAndGroups;
    private final List<Student> listOfStudents = new ArrayList<>();
    private final MarkTable markTable = new MarkTable();

    public UniversityService(Map<Faculty, List<Integer>> mapOfFacultiesAndGroups) throws NoFacultiesInUniversityException,
            NoGroupsOnFacultyException {
        if(mapOfFacultiesAndGroups == null || mapOfFacultiesAndGroups.isEmpty()) {
            throw new NoFacultiesInUniversityException("No faculties in University. There should be at least one.");
        }
        for(Faculty faculty: mapOfFacultiesAndGroups.keySet()) {
            if(mapOfFacultiesAndGroups.get(faculty).isEmpty()) {
                throw new NoGroupsOnFacultyException("No groups on faculty. There should be at least one.");
            }
        }
        this.mapOfFacultiesAndGroups = mapOfFacultiesAndGroups;
    }

    public List<Student> getListOfStudents() {
        return listOfStudents;
    }

    public Student enrollApplicant(String name, String surname, int group, Faculty faculty)
            throws NoSuchFacultyInUniversityException, NoSuchGroupOnFacultyException {
        if (!mapOfFacultiesAndGroups.containsKey(faculty)) {
            throw new NoSuchFacultyInUniversityException("There is no such faculty in this University." +
                    " Unable to add student.");
        }
        if(!mapOfFacultiesAndGroups.get(faculty).contains(group)) {
            throw new NoSuchGroupOnFacultyException("There is no such group on faculty. " +
                    "Unable to add student.");
        }
        Student newStudent = new Student(name, surname, group, faculty);
        listOfStudents.add(newStudent);
        return newStudent;
    }

    public void gradeStudentInSubject(Student student, Subject subject, Integer mark) throws InvalidMarkException {
        markTable.gradeStudentInSubject(student, subject, mark);
    }

    public double getAverageMarkForAllSubjectsOfStudent(Student student) throws NoSubjectsForStudentException {
        Map<Subject, Integer> studentMarks = markTable.getMarksForStudent(student);
        double sumOfMarks = 0.0;
        for(Integer mark : studentMarks.values()) {
            sumOfMarks += mark;
        }
        return sumOfMarks / studentMarks.size();
    }

    public double getAverageMarkForSubjectInCurrentGroupOfFaculty(Subject subject, int group, Faculty faculty)
            throws NoStudentsInGroupException {
        checkGroupIsNotEmpty(group);
        List<Student> studentsOfCurrentFacultyGroup = listOfStudents.stream()
                .filter(student -> student.getFaculty() == faculty && student.getGroup() == group)
                .collect(Collectors.toList());
        double sumOfMarks = 0.0;
        int numberOfMarks = 0;
        for(Student student : studentsOfCurrentFacultyGroup) {
            if(markTable.getStudentMarkBySubject(student, subject) != null) {
                sumOfMarks += markTable.getStudentMarkBySubject(student, subject);
                numberOfMarks++;
            }
        }
        return sumOfMarks / numberOfMarks;
    }

    public double getAverageMarkForSubjectByUniversity(Subject subject) {
        return markTable.getAllMarksBySubject(subject).stream()
                .mapToInt(Integer::intValue).summaryStatistics().getAverage();
    }

    public void checkGroupIsNotEmpty(int group) throws NoStudentsInGroupException {
        if(listOfStudents.stream().noneMatch(student -> student.getGroup() == group)) {
            throw new NoStudentsInGroupException("There are no students in such group. Should be at least one.");
        }
    }
}