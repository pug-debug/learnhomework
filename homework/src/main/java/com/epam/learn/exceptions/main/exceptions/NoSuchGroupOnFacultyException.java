package com.epam.learn.exceptions.main.exceptions;

public class NoSuchGroupOnFacultyException extends Exception {
    public NoSuchGroupOnFacultyException(String message) {
        super(message);
    }
}
