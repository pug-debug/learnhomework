package com.epam.learn.exceptions.main.exceptions;

public class NoSubjectsForStudentException extends Exception{
    public NoSubjectsForStudentException(String message) {
        super(message);
    }
}
