package com.epam.learn.exceptions.main.exceptions;

public class NoFacultiesInUniversityException extends Exception{
    public NoFacultiesInUniversityException(String message) {
        super(message);
    }
}
