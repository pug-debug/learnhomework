package com.epam.learn.exceptions.main;

import com.epam.learn.exceptions.main.universityEntities.Faculty;

import java.util.Objects;

public class Student {
    private final String name;
    private final String surname;
    private final int group;
    private final Faculty faculty;

    public Student(String name, String surname, int group, Faculty faculty) {
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.faculty = faculty;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getGroup() {
        return group;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", group=" + group +
                ", faculty=" + faculty +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return group == student.group && name.equals(student.name) && surname.equals(student.surname) && faculty == student.faculty;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, group, faculty);
    }
}
