package com.epam.learn.exceptions.main;

import com.epam.learn.exceptions.main.exceptions.*;
import com.epam.learn.exceptions.main.services.UniversityService;
import com.epam.learn.exceptions.main.universityEntities.Faculty;
import com.epam.learn.exceptions.main.universityEntities.Subject;

import java.util.*;

public class Builder {
    public static UniversityService bsu = null;
    public static Student bsu_student1 = null;
    public static Student bsu_student2 = null;

    public static UniversityService bmu = null;
    public static Student bmu_student1 = null;
    public static Student bmu_student2 = null;
    public static Student bmu_student3 = null;


    public static void createBsuUniversity() {
        try {
            Map<Faculty, List<Integer>> mapOfFacultiesAndGroupsBsu = new HashMap<>();
            mapOfFacultiesAndGroupsBsu.put(Faculty.MMF, Arrays.asList(201, 101));
            mapOfFacultiesAndGroupsBsu.put(Faculty.BIO, Arrays.asList(203, 102));
            bsu = new UniversityService(mapOfFacultiesAndGroupsBsu);
        } catch (NoGroupsOnFacultyException | NoFacultiesInUniversityException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void enrollBsuStudents() {
        try {
            assert bsu != null;
            bsu_student1 = bsu.enrollApplicant("Ivan", "Ivanov", 101, Faculty.MMF);
            bsu_student2 = bsu.enrollApplicant("Elizaveta", "Sidorova", 201, Faculty.MMF);
            bsu.enrollApplicant("Petr", "Petrov", 101, Faculty.MMF);
            bsu.enrollApplicant("Evgeniya", "Zhuk", 201, Faculty.MMF);
        } catch (NoSuchFacultyInUniversityException | NoSuchGroupOnFacultyException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createBmuUniversity() {
        try {
            HashMap<Faculty, List<Integer>> mapOfFacultiesAndGroupsBmu = new HashMap<>();
            mapOfFacultiesAndGroupsBmu.put(Faculty.GEO, Arrays.asList(408, 509));
            bmu = new UniversityService(mapOfFacultiesAndGroupsBmu);
        } catch (NoGroupsOnFacultyException | NoFacultiesInUniversityException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void enrollBmuStudents() {
        try {
            assert bmu != null;
            bmu_student1 = bmu.enrollApplicant("Vitali", "Kohovets", 408, Faculty.GEO);
            bmu_student2 = bmu.enrollApplicant("Anastasiya", "Zinkevich", 408, Faculty.GEO);
            bmu_student3 = bmu.enrollApplicant("Atryom", "Rylacch", 408, Faculty.GEO);
        } catch (NoSuchFacultyInUniversityException | NoSuchGroupOnFacultyException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void gradeBsuStudentInASubjectManually() {
        for(Student student : bsu.getListOfStudents()) {
            System.out.printf("Grading student %s %s: \n", student.getName(), student.getSurname());
            while (true) {
                try {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("Enter subject name: ");
                    String subjectName = scanner.nextLine();
                    System.out.println("Enter mark: ");
                    int subjectMark = scanner.nextInt();
                    bsu.gradeStudentInSubject(student,
                            Subject.valueOf(subjectName.toUpperCase(Locale.ROOT)),
                            subjectMark);
                } catch (IllegalArgumentException e) {
                    System.out.println("This subject does not exist. Try again.");
                    continue;
                } catch (InvalidMarkException e) {
                    System.out.println(e.getMessage());
                    continue;
                } catch (InputMismatchException e) {
                    System.out.println("Wrong mark format! Enter integer 0-10.");
                    continue;
                }
                break;
            }
        }
    }

    public static void gradeBmuStudentInASubject() {
        try{
            bmu.gradeStudentInSubject(bmu_student1, Subject.BIOLOGY, 9);
            bmu.gradeStudentInSubject(bmu_student2, Subject.PSYCHOLOGY, 7);
        } catch (InvalidMarkException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void showStudentsAverageMark() {
        try {
            // student with subjects
            System.out.printf("%s %s average mark for all subjects: " +
                    bsu.getAverageMarkForAllSubjectsOfStudent(bsu_student1) + "\n", bsu_student1.getName(),
                    bsu_student1.getSurname());

            // student without subjects
            bmu.getAverageMarkForAllSubjectsOfStudent(bmu_student3);
        } catch (NoSubjectsForStudentException e) {
            System.out.println(e.getMessage() + "\n");
        }
    }

    public static void showSubjectAverageMarkInGroup() {
        try {
            // group with students
            System.out.println("Math average mark in 101 group MMF faculty: " +
                    bsu.getAverageMarkForSubjectInCurrentGroupOfFaculty(Subject.MATH, 101, Faculty.MMF));

            // group without students
            System.out.println(bmu.getAverageMarkForSubjectInCurrentGroupOfFaculty(Subject.BIOLOGY,
                    509, Faculty.BIO));
        } catch (NoStudentsInGroupException e) {
            System.out.println(e.getMessage() + "\n");
        }
    }

    public static void createUniversityWithoutFaculties() {
        try {
            Map<Faculty, List<Integer>> mapOfFacultiesAndGroupsBseu= new HashMap<>();
            new UniversityService(mapOfFacultiesAndGroupsBseu);
        } catch (NoFacultiesInUniversityException | NoGroupsOnFacultyException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createUniversityWithoutGroups() {
        try {
            Map<Faculty, List<Integer>> mapOfFacultiesAndGroupsMslu= new HashMap<>();
            mapOfFacultiesAndGroupsMslu.put(Faculty.FEN, new ArrayList<>());
            new UniversityService(mapOfFacultiesAndGroupsMslu);
        } catch (NoGroupsOnFacultyException | NoFacultiesInUniversityException e) {
            System.out.println(e.getMessage());
        }
    }
}
