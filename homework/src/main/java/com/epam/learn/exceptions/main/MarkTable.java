package com.epam.learn.exceptions.main;

import com.epam.learn.exceptions.main.exceptions.InvalidMarkException;
import com.epam.learn.exceptions.main.exceptions.NoSubjectsForStudentException;
import com.epam.learn.exceptions.main.universityEntities.Subject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MarkTable implements Table {
    private final Map<Student, Map<Subject, Integer>> listOfStudentMarks = new HashMap<>();

    @Override
    public void gradeStudentInSubject(Student student, Subject subject, Integer mark) throws InvalidMarkException {
        if(mark < 0 || mark > 10) {
            throw new InvalidMarkException("Invalid mark! Mark should be 0-10.");
        }
        if(!listOfStudentMarks.containsKey(student)) {
            Map<Subject, Integer> subjectMarks = new HashMap<>();
            listOfStudentMarks.put(student, subjectMarks);
        }
        listOfStudentMarks.get(student).put(subject, mark);
    }

    @Override
    public Map<Subject, Integer> getMarksForStudent(Student student) throws NoSubjectsForStudentException {
        if(listOfStudentMarks.get(student) == null) {
            throw new NoSubjectsForStudentException("This student doesn't study any subjects." +
                    " There should be at least one.");
        }
        return listOfStudentMarks.get(student);
    }

    @Override
    public Integer getStudentMarkBySubject(Student student, Subject subject) {
        if(listOfStudentMarks.get(student) == null) {
            return null;
        }
        return listOfStudentMarks.get(student).get(subject);
    }

    @Override
    public List<Integer> getAllMarksBySubject(Subject subject) {
        List<Integer> marksBySubject = new ArrayList<>();
        for(Student currentStudent : listOfStudentMarks.keySet()) {
            if(listOfStudentMarks.get(currentStudent).containsKey(subject)) {
                marksBySubject.add(listOfStudentMarks.get(currentStudent).get(subject));
            }
        }
        return marksBySubject;
    }
}
