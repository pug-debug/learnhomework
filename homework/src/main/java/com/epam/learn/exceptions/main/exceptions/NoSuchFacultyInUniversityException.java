package com.epam.learn.exceptions.main.exceptions;

public class NoSuchFacultyInUniversityException extends Exception {
    public NoSuchFacultyInUniversityException(String message) {
        super(message);
    }
}
