package com.epam.learn.exceptions.main.exceptions;

public class NoStudentsInGroupException extends Exception{
    public NoStudentsInGroupException(String message) {
        super(message);
    }
}
