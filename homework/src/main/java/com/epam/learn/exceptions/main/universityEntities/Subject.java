package com.epam.learn.exceptions.main.universityEntities;

public enum Subject {
    MATH, BIOLOGY, PSYCHOLOGY, POLITICAL_SCIENCE
}
