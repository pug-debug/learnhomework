package com.epam.learn.exceptions.main;

import com.epam.learn.exceptions.main.universityEntities.Subject;

public class TestClass {
    public static void main(String[] args) {
        Builder.createBsuUniversity();
        Builder.enrollBsuStudents();
        Builder.createBmuUniversity();
        Builder.enrollBmuStudents();

        Builder.gradeBsuStudentInASubjectManually();
        Builder.gradeBmuStudentInASubject();

        Builder.showStudentsAverageMark();

        Builder.showSubjectAverageMarkInGroup();

        System.out.println("Math average mark in university: " +
                Builder.bsu.getAverageMarkForSubjectByUniversity(Subject.MATH));

        Builder.createUniversityWithoutGroups();

        Builder.createUniversityWithoutFaculties();
    }
}