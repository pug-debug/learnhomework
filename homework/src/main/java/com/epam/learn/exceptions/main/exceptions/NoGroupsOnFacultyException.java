package com.epam.learn.exceptions.main.exceptions;

public class NoGroupsOnFacultyException extends Exception{
    public NoGroupsOnFacultyException(String message) {
        super(message);
    }
}
