package com.epam.learn.classes.main;

import java.util.*;

public class Student {
    private final String id;
    private String surname;
    private String name;
    private String patronymic;
    private Calendar birthDate;
    private String address;
    private String phoneNumber;
    private Faculty faculty;
    private int course;
    private int group;

    public Student(String surname, String name, String patronymic, Calendar birthDate, String address,
                   String phoneNumber, Faculty faculty, int course, int group) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
        this.id = UUID.randomUUID().toString();
        University.addStudent(this);
    }

    public Student(String surname, String name, String patronymic) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.id = UUID.randomUUID().toString();
        University.addStudent(this);
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getCourse() {
        return course;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getGroup() {
        return group;
    }

    public String toString() {
        return String.format("<Student{id = %s, surname = %s, name = %s, patronymic = %s, group = %s}>",
                id, surname, name, patronymic, group);
    }


}
