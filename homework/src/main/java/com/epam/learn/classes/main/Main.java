package com.epam.learn.classes.main;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        Student karinaLitskevich = new Student("Litskevich", "Karina", "Vladimirovna",
                new GregorianCalendar(1996, Calendar.OCTOBER,22), "Minsk, Esenina str. 45",
                "+375 44 688 04 93", Faculty.MMF, 2, 209);

        Student ivanPopov = new Student("Popov", "Ivan", "Sergeevich",
                new GregorianCalendar(1994, Calendar.MARCH, 8), "Brest, Sovetskaya str. 77",
                "+375 29 275 39 09", Faculty.GEO, 4, 401);

        Student ekaterinaIvanova = new Student("Ivanova", "Ekaterina", "Andreevna",
                new GregorianCalendar(1999, Calendar.FEBRUARY, 13), "Minsk, Vostochnaya str. 88",
                "+375 29 114 56 82", Faculty.GEO, 2, 202);

        Student olgaIvanova = new Student("Ivanova", "Olga", "Sergeevna",
                new GregorianCalendar(1991, Calendar.AUGUST, 2), "Minsk, Vostochnaya str. 81",
                "+375 22 114 56 82", Faculty.GEO, 2, 202);

        Student igorPetrovich = new Student("Petrovich", "Igor", "Viktorovich",
                new GregorianCalendar(1998, Calendar.DECEMBER, 31), "Minsk, Lenina str. 54",
                "+375 29 345-67-89", Faculty.MMF, 2, 209);

        Student tatianaVolchok = new Student("Volchok", "Tatiana", "Sergeenvna",
                new GregorianCalendar(1996, Calendar.JULY, 30), "Minsk, Kamennogorskaya str. 56",
                "+375 29 473 89 12", Faculty.BIO, 4, 405);

        Student elenaBosko = new Student("Bosko", "Elena", "Mihailovna",
                new GregorianCalendar(1995, Calendar.MAY, 6), "Grodno, Ozheshko str. 24",
                "+375 44 783 09 33", Faculty.GEO, 3, 303);

        Student valeryLitskevich = new Student("Litskevich", "Valery", "Leonidovich",
                new GregorianCalendar(1994, Calendar.OCTOBER, 15), "Minsk, Moskovskaya str. 33",
                "+375 44 376 22 56", Faculty.MMF, 1, 201);

        System.out.printf("List of GEO faculty students:\n %s \n", University.getListOfStudentByFaculty(Faculty.GEO));
        System.out.println("=================================");
        System.out.printf("Table of students by group and faculty: \n %s \n", University.getTableOfStudentsByFacultyAndGroup());
        System.out.println("=================================");
        System.out.printf("List of students born after 1997: \n %s \n", University.getListOfStudentsBornAfterYear(1997));
        System.out.println("=================================");
        System.out.printf("List of students of group 202: \n %s \n", University.getListOfStudentsByGroup(202));
    }
}
