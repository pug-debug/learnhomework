package com.epam.learn.classes.main;

import java.util.*;

public class University {
    private static final List<Student> students = new ArrayList<>();

    public static void addStudent(Student student) {
        students.add(student);
    }

    public static List<Student> getStudents() {
        return students;
    }

    public static List<Student> getListOfStudentByFaculty(Faculty faculty) {
        List<Student> listOfStudentsByFaculty = new ArrayList<>();
        for (Student student : students) {
            if (student.getFaculty() == faculty) {
                listOfStudentsByFaculty.add(student);
            }
        }
        listOfStudentsByFaculty.sort(
                Comparator.comparing(Student::getCourse)
                        .thenComparing(Student::getSurname)
                        .thenComparing(Student::getName));
        return listOfStudentsByFaculty;
    }

    public static List<Student> getListOfStudentsBornAfterYear(int year) {
        List<Student> listOfStudentsBornAfterYear = new ArrayList<>();
        for (Student student : students) {
            if (student.getBirthDate().after(new GregorianCalendar(year, Calendar.DECEMBER, 31))) {
                listOfStudentsBornAfterYear.add(student);
            }
        }
        listOfStudentsBornAfterYear.sort(Comparator.comparing(Student::getBirthDate)
                .thenComparing(Student::getSurname)
                .thenComparing(Student::getName)
                .thenComparing(Student::getPatronymic));
        return listOfStudentsBornAfterYear;
    }

    public static List<Student> getListOfStudentsByGroup(int group) {
        List<Student> listOfStudentsByGroup = new ArrayList<>();
        for (Student student : students) {
            if (student.getGroup() == group) {
                listOfStudentsByGroup.add(student);
            }
        }
        listOfStudentsByGroup.sort(Comparator.comparing(Student::getSurname)
                .thenComparing(Student::getName).thenComparing(Student::getPatronymic));
        return listOfStudentsByGroup;
    }

    public static HashMap<Faculty, HashMap<Integer, ArrayList<Student>>> getTableOfStudentsByFacultyAndGroup() {
        HashMap<Faculty, HashMap<Integer, ArrayList<Student>>> listOfStudentsByFacultyAndGroup = new HashMap<>();
        for (Student student : students) {
            if (student.getFaculty() != null) {
                HashMap<Integer, ArrayList<Student>> studentsOfFaculty = listOfStudentsByFacultyAndGroup
                        .get(student.getFaculty());
                if (studentsOfFaculty != null) {
                    addStudentToFacultyAndCourse(student, studentsOfFaculty);
                } else {
                    addNewStudent(student,listOfStudentsByFacultyAndGroup);
                }
            }
        }
        return listOfStudentsByFacultyAndGroup;
    }

    private static void addStudentToFacultyAndCourse
            (Student student, HashMap<Integer, ArrayList<Student>> studentsOfFaculty) {
        if (studentsOfFaculty.containsKey(student.getCourse())) {
            ArrayList<Student> studentsOfCourse = studentsOfFaculty.get(student.getCourse());
            studentsOfCourse.add(student);
        } else {
            ArrayList<Student> studentsOfCourse = new ArrayList<>();
            studentsOfCourse.add(student);
            studentsOfFaculty.put(student.getCourse(), studentsOfCourse);
        }
    }

    private static void addNewStudent
            (Student student, HashMap<Faculty, HashMap<Integer, ArrayList<Student>>> listOfStudentsByFacultyAndGroup ) {
        HashMap<Integer, ArrayList<Student>> studentsOfCourse = new HashMap<>();
        ArrayList<Student> facultyStudents = new ArrayList<>();
        facultyStudents.add(student);
        studentsOfCourse.put(student.getCourse(), facultyStudents);
        listOfStudentsByFacultyAndGroup.put(student.getFaculty(), studentsOfCourse);
    }
}
