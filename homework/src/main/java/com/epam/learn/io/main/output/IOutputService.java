package com.epam.learn.io.main.output;

public interface IOutputService {
    void generateOutput(String directoryTree);
}