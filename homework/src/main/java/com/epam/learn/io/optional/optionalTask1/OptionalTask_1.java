package com.epam.learn.io.optional.optionalTask1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OptionalTask_1 {
    public static void main(String[] args) throws IOException {
        String pathToOriginalFile = generateUniversalPath("src/main/java/com/epam/learn/io/optional/optionalTask1/fileWithIntegers.txt");
        String pathToSortedFile = generateUniversalPath("src/main/java/com/epam/learn/io/optional/optionalTask1/fileWithSortedIntegers.txt");
        generateFileWithRandomIntegers(pathToOriginalFile);
        sortIntegersFromFile(pathToOriginalFile, pathToSortedFile);
    }

    private static String generateUniversalPath(String path) {
        return path.replaceAll("/", File.separator);
    }

    private static void generateFileWithRandomIntegers(String path) {
        try(PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
            Random random = new Random();
            int integerCount = 0;
            while (integerCount < 100) {
                writer.println(random.nextInt(1000));
                integerCount++;
            }
            System.out.println("File is generated successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sortIntegersFromFile(String pathToOriginalFile, String pathToSortedFile) {
        List<Integer> integersFromFile = new ArrayList<>();
        try(FileReader reader = new FileReader(pathToOriginalFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            FileWriter writer = new FileWriter(pathToSortedFile);
            BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedReader.lines().mapToInt(Integer::parseInt).forEach(integersFromFile::add);
            integersFromFile.sort(Integer::compareTo);

            for(int currentInteger : integersFromFile) {
                String currentIntToString = String.valueOf(currentInteger);
                bufferedWriter.write(currentIntToString);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("File is sorted successfully");
    }
}