package com.epam.learn.io.main.output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileIOutputService implements IOutputService {

    @Override
    public void generateOutput(String directoryTree) {
        try(FileWriter writer = new FileWriter
                ("src/main/java/com/epam/learn/io/main/files/tree.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedWriter.write(directoryTree);
            System.out.println("Directory tree is written in file successfully.");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
