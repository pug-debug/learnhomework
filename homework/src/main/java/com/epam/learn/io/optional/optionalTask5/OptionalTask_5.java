package com.epam.learn.io.optional.optionalTask5;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OptionalTask_5 {
    public static void main(String[] args) {
        String pathToFileWithMarks = generateUniversalPath("src/main/java/com/epam/learn/io/optional/optionalTask5/markTable.txt");
        String pathToFileWithExcellentStudents = generateUniversalPath("src/main/java/com/epam/learn/io/optional/optionalTask5/markTableWithExcellentStudents.txt");

        try (FileReader reader = new FileReader(pathToFileWithMarks);
             BufferedReader bufferedReader = new BufferedReader(reader);
             FileWriter writer = new FileWriter(pathToFileWithExcellentStudents);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
             String line;
             while ((line = bufferedReader.readLine()) != null) {
                 double averageMark = getAverageMark(line);
                 outlineExcellentStudents(averageMark, line,bufferedWriter);
             }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String generateUniversalPath(String path) {
        return path.replaceAll("/", File.separator);
    }

    private static void outlineExcellentStudents(double averageMark, String line, BufferedWriter bufferedWriter) throws IOException {
        if (averageMark >= 7.0) {
            String newLine = line.substring(0, line.indexOf(" ")).toUpperCase() + line.substring(line.indexOf(" "));
            bufferedWriter.write(newLine);
        } else {
            bufferedWriter.write(line);
        }
        bufferedWriter.newLine();
    }

    private static double getAverageMark(String line) {
        int index = line.indexOf(':') + 2;
        String[] marks = line.substring(index).split(",");
        List<Integer> marksList = new ArrayList<>();
        Arrays.stream(marks).mapToInt(Integer::parseInt).forEach(marksList::add);
        return marksList.stream().mapToDouble(d -> d).average().orElse(0);
    }
}