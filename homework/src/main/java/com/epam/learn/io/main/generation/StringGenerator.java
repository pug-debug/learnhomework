package com.epam.learn.io.main.generation;

import com.epam.learn.io.main.exceptions.EmptyPathException;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;

public abstract class StringGenerator {
    public final String path;

    protected StringGenerator(String path) throws EmptyPathException {
        checkPath(path);
        this.path = path;
    }

    public abstract String generateString();

    private void checkPath(String path) throws EmptyPathException {
        if(path == null || path.isEmpty()) {
            throw new EmptyPathException("You entered empty path.");
        }
        if(!Files.exists(Path.of(path))) {
            throw new InvalidPathException(path, "\n You entered invalid path.");
        }
    }
}