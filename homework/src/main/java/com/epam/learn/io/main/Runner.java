package com.epam.learn.io.main;

import com.epam.learn.io.main.exceptions.EmptyPathException;
import com.epam.learn.io.main.generation.StringGenerator;
import com.epam.learn.io.main.generation.TxtScanner;
import com.epam.learn.io.main.generation.Walker;
import com.epam.learn.io.main.output.ConsoleOutputService;
import com.epam.learn.io.main.output.FileIOutputService;
import com.epam.learn.io.main.output.IOutputService;

import java.nio.file.InvalidPathException;

public class Runner {
    public static void run(String path, WayOfSorting wayOfSorting) {
        try {
            if (path.endsWith(".txt")) {
                run(new TxtScanner(path), new ConsoleOutputService());
            } else {
                run(new Walker(path, wayOfSorting), new FileIOutputService());
            }
        } catch (InvalidPathException e) {
            System.out.println("Invalid path was entered. This file or directory doesn't exist.");
            System.exit(1);
        } catch (EmptyPathException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    private static void run(StringGenerator generator, IOutputService outputService) {
            outputService.generateOutput(generator.generateString());
    }
}