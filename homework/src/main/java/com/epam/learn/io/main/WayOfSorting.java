package com.epam.learn.io.main;

public enum WayOfSorting {
    ALPHABET, BY_FOLDERS
}
