package com.epam.learn.io.main.generation;

import com.epam.learn.io.main.exceptions.EmptyPathException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TxtScanner extends StringGenerator {
    public TxtScanner(String path) throws EmptyPathException {
        super(path);
    }

    @Override
    public String generateString() {
        StringBuilder builder = new StringBuilder();
        List<String> lines = new ArrayList<>();
        try (FileReader reader = new FileReader(path);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            lines = bufferedReader.lines().collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        builder.append(Paths.get(path).getFileName().toString()).append(" info: \n").
                append("Amount of folders: ").append(countNumberOfFolders(lines)).append("\n").
                append("Amount of files: ").append(countNumberOfFiles(lines)).append("\n").
                append("Average amount of files in folder: ").append(getAverageAmountOfFilesInFolder(lines)).append("\n").
                append("Average file name length: ").append(getAverageFileNameLength(lines)).append("\n");

        return builder.toString();
    }

    private int countNumberOfFolders(List<String> lines) {
        return (int) lines.stream().filter(p -> p.startsWith("|-")).count();
    }

    private int countNumberOfFiles(List<String> lines) {
        return (int) lines.stream().filter(p -> p.startsWith("| ")).count();
    }

    private int getAverageAmountOfFilesInFolder(List<String> lines) {
        List<Integer> numberOfFilesInFolders = new ArrayList<>();
        List<String> folders = lines.stream().filter(p -> p.startsWith("|-")).collect(Collectors.toList());
        Map<String, List<String>> foldersWithFiles = new HashMap<>();
        for (String folder : folders) {
            int folderLevel = getFolderLevel(folder);
            int fileCounter = 0;
            int currentFolderIndex = lines.indexOf(folder);
            foldersWithFiles.put(folder, new ArrayList<>());
            for (int i = currentFolderIndex + 1; i < lines.size(); i++) {
                if (!lines.get(i).startsWith("|-") && getNumberOfSpacesInTheBeginningOfFileName(lines.get(i)) - folderLevel - 2 == 0) {
                    fileCounter++;
                    foldersWithFiles.get(folder).add(lines.get(i));
                }
                if (getFolderLevel(lines.get(i)) == folderLevel) {
                    break;
                }
            }
            numberOfFilesInFolders.add(fileCounter);
        }
        return (int) numberOfFilesInFolders.stream().mapToDouble(d -> d).average().orElse(0);
    }

    private int getFolderLevel(String folder) {
        long count2 = folder.chars().filter(ch -> ch == '-').count();
        return (int) count2;
    }

    private int getNumberOfSpacesInTheBeginningOfFileName(String fileName) {
        int spaces = 0;
        for (int i = 1; i < fileName.length(); i++) {
            char ch = fileName.charAt(i);
            if (ch == ' ') {
                spaces++;
            } else break;
        }
        return spaces;
    }

    private int getAverageFileNameLength(List<String> lines) {
        List<Integer> fileNameLengths = new ArrayList<>();
        for (String line : lines) {
            if (line.startsWith("| ")) {
                fileNameLengths.add(line.replaceAll(" ", "").substring(1).length());
            }
        }
        return (int) fileNameLengths.stream().mapToDouble(d -> d).average().orElse(-0);
    }
}