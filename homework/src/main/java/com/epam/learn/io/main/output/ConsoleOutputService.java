package com.epam.learn.io.main.output;

public class ConsoleOutputService implements IOutputService {
    @Override
    public void generateOutput(String directoryTree) {
        System.out.println(directoryTree);
    }
}