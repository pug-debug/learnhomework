package com.epam.learn.io.optional.optionalTask2;

import java.io.*;

public class OptionalTask_2 {
    public static void main(String[] args) {
        String pathToReadFile = generateUniversalPath("src/main/java/com/epam/learn/exceptions/main/MarkTable.java");
        String pathToCopiedFile = generateUniversalPath("src/main/java/com/epam/learn/io/optional/optionalTask2/copiedJavaFile");
        String pathToFileWithReplacements = generateUniversalPath("src/main/java/com/epam/learn/io/optional/optionalTask2/fileWithReplacements");

        copyJavaFileToTxtFile(pathToReadFile, pathToCopiedFile);
        replaceWord(pathToCopiedFile, pathToFileWithReplacements, "public", "private");


    }
    private static String generateUniversalPath(String path) {
        return path.replaceAll("/", File.separator);
    }

    private static void copyJavaFileToTxtFile(String pathToReadFile, String pathToWriteFile) {
        try(FileReader reader = new FileReader(pathToReadFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            FileWriter writer = new FileWriter(pathToWriteFile);
            BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                bufferedWriter.write(line, 0, line.length());
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void replaceWord(String pathToCopiedFile, String pathToFileWithReplacements,
                                    String wordToReplace, String replacementWord) {
        try(FileReader reader = new FileReader(pathToCopiedFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            FileWriter writer = new FileWriter(pathToFileWithReplacements);
            BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if(line.contains(wordToReplace) && !line.contains(" class ")) {
                    line = line.replaceAll(wordToReplace, replacementWord);
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                } else {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}