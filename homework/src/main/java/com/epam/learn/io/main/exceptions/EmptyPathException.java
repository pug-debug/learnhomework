package com.epam.learn.io.main.exceptions;

public class EmptyPathException extends Exception {
    public EmptyPathException(String message) {
        super(message);
    }
}
