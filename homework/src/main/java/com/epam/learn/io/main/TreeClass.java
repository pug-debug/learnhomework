package com.epam.learn.io.main;

import com.epam.learn.io.main.exceptions.EmptyPathException;
import org.apache.commons.cli.*;

public class TreeClass {
    public static void main(String[] args) throws EmptyPathException {
        CommandLine commandLineArgument = null;
        try {
            commandLineArgument = generateCommandLineArguments(args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        WayOfSorting wayOfSorting = defineWayOfSorting(commandLineArgument);

        Runner.run(commandLineArgument.getOptionValue("path"), wayOfSorting);
    }

    private static CommandLine generateCommandLineArguments(String[] args) throws ParseException {
        CommandLine commandLineArgument;
        Option wayOfSorting = new Option("s", "sort", true, "Sort");
        wayOfSorting.setArgs(1);
        wayOfSorting.setArgName("sort");

        Option path = new Option("p", "path", true, "Path to file");
        path.setRequired(true);
        path.setArgs(1);
        path.setArgName("path");

        Options options = new Options();
        options.addOption(wayOfSorting);
        options.addOption(path);

        CommandLineParser parser = new DefaultParser();
        commandLineArgument = parser.parse(options, args);
        return commandLineArgument;
    }

    private static WayOfSorting defineWayOfSorting(CommandLine commandLineArgument) {
        WayOfSorting wayOfSorting = null;
        if(commandLineArgument.hasOption('s')) {
            if (commandLineArgument.getOptionValue('s').equals("f")) {
                wayOfSorting = WayOfSorting.BY_FOLDERS;
            }
            else if (commandLineArgument.getOptionValue('s').equals("a")) {
                wayOfSorting = WayOfSorting.ALPHABET;
            } else {
                System.out.println("Not valid argument for option -s. Should be 'f' or 'a' for sorting.");
                System.exit(1);
            }
        } else {
            wayOfSorting = WayOfSorting.ALPHABET;
        }
        return wayOfSorting;
    }
}