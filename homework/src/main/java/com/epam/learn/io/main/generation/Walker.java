package com.epam.learn.io.main.generation;

import com.epam.learn.io.main.WayOfSorting;
import com.epam.learn.io.main.exceptions.EmptyPathException;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;

public class Walker extends StringGenerator {
    private static final int indentLength = 1;
    private final WayOfSorting wayOfSorting;

    public Walker(String path, WayOfSorting wayOfSorting) throws EmptyPathException {
        super(path);
        this.wayOfSorting = wayOfSorting;
    }

    @Override
    public String generateString() {
        File[] files = new File(path).listFiles();
        StringBuilder builder = new StringBuilder();
        builder.append(Paths.get(path).getFileName().toString()).append("\n");
        return showFiles(files, indentLength, builder);
    }

    private String showFiles(File[] files, int indentLength, StringBuilder builder) {
        sortDirectory(files);
        try {
            assert files != null;
            for (File file : files) {
                if (file.isDirectory()) {
                    String indentForFolder = "|" + "-".repeat(indentLength);
                    builder.append(indentForFolder).append(file.getName()).append("\n");
                    builder.append(showFiles(file.listFiles(), indentLength + 2, new StringBuilder()));
                } else {
                    String indentForFile = "|" + " ".repeat(indentLength);
                    builder.append(indentForFile).append(file.getName()).append("\n");
                }
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
        return builder.toString();
    }

    private void sortDirectory(File[] files) {
        if (wayOfSorting == WayOfSorting.ALPHABET) {
            Arrays.sort(files, Comparator.comparing(f -> f.toString().toLowerCase()));
        }
        if(wayOfSorting == WayOfSorting.BY_FOLDERS) {
            Arrays.sort(files, (f1, f2) -> {
                if (f1.isDirectory() && !f2.isDirectory()) {
                    return -1;
                } else if (!f1.isDirectory() && f2.isDirectory()) {
                    return 1;
                } else {
                    return f1.compareTo(f2);
                }
            });
        }
    }
}