package com.epam.learn.collections.main.taxi.cars;

import com.epam.learn.collections.main.taxi.types.CarModel;
import java.util.Objects;

public abstract class Car {
    private final CarModel model;
    private int price;
    private final int fuelConsumption;
    private int minSpeed;
    private int maxSpeed;

    public Car(CarModel model, int price, int fuelConsumption, int minSpeed, int maxSpeed) throws Exception {
        this.model = model;
        this.price = price;
        this.fuelConsumption = fuelConsumption;
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
        checkParameters();
    }

    public int getPrice() {
        return price;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public int getMinSpeed() {
        return minSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public CarModel getModel() { return model; }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setMinSpeed(int minSpeed) {
        this.minSpeed = minSpeed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                ", fuelConsumption=" + fuelConsumption +
                ", minSpeed=" + minSpeed +
                ", maxSpeed=" + maxSpeed +
                '}' + '\n';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return price == car.price && fuelConsumption == car.fuelConsumption && minSpeed == car.minSpeed
                && maxSpeed == car.maxSpeed && model == car.model;
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, price, fuelConsumption, minSpeed, maxSpeed);
    }

    public abstract void checkParameters() throws Exception;
    public abstract void startTheCar();
}
