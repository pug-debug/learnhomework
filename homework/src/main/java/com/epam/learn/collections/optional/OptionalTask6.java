package com.epam.learn.collections.optional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class OptionalTask6 {
    public static void main(String[] args) throws IOException {
        List<String> poemLines = Files.readAllLines(Paths.get("src/main/java/com/epam/learn/collections/optional/Poem.txt"));
        Collections.sort(poemLines);
        System.out.println(poemLines);
    }
}
