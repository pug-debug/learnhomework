package com.epam.learn.collections.main.taxi.exceptions;

public class IncorrectModelException extends Exception{
    public IncorrectModelException(String message) {
        super(message);
    }
}
