package com.epam.learn.collections.main.taxi.cars;

import com.epam.learn.collections.main.taxi.exceptions.IncorrectCarPriceException;
import com.epam.learn.collections.main.taxi.exceptions.IncorrectModelException;
import com.epam.learn.collections.main.taxi.types.CarModel;

public class EconomyCar extends Car{

    public EconomyCar(CarModel model, int price, int fuelConsumption, int minSpeed, int maxSpeed) throws Exception {
        super(model, price, fuelConsumption, minSpeed, maxSpeed);
    }

    public void checkParameters() throws IncorrectCarPriceException, IncorrectModelException {
        if (getPrice() > 5000) {
            throw new IncorrectCarPriceException("You entered wrong price. Economy cars cost less than 5000$");
        }
        if(getModel() != CarModel.VOLKSWAGEN && getModel() != CarModel.RENAULT && getModel() != CarModel.HONDA) {
            System.out.println(getModel());
            throw new IncorrectModelException("This car type can't be Economy class.");
        }
    }

    @Override
    public void startTheCar() {
        System.out.println("Wroom.... wrrrr... wait a minute and try again.");
        this.setMaxSpeed(this.getMaxSpeed()-10);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EconomyCar)) return false;
        return super.equals(o);
    }

    @Override
    public String toString() {
        return super.toString().replace("}", ", class = EconomyCar}");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
