package com.epam.learn.collections.main.taxi.cars;

import com.epam.learn.collections.main.taxi.carFeatures.ArmoredGlass;
import com.epam.learn.collections.main.taxi.carFeatures.Engine;
import com.epam.learn.collections.main.taxi.carFeatures.Phone;
import com.epam.learn.collections.main.taxi.exceptions.IncorrectCarPriceException;
import com.epam.learn.collections.main.taxi.exceptions.IncorrectModelException;
import com.epam.learn.collections.main.taxi.types.CarModel;

public class BusinessCar extends Car {
    private final Phone personalPhone;
    private ArmoredGlass typeOfGlassProtection;
    private final String personalDriverName;

    public BusinessCar(CarModel model, int price, int fuelConsumption, int minSpeed, int maxSpeed, Phone personalPhone,
                       ArmoredGlass typeOfGlassProtection, String personalDriverName) throws Exception {
        super(model, price, fuelConsumption, minSpeed, maxSpeed);
        this.personalPhone = personalPhone;
        this.typeOfGlassProtection = typeOfGlassProtection;
        this.personalDriverName = personalDriverName;
    }

    @Override
    public void checkParameters() throws IncorrectCarPriceException, IncorrectModelException {
        if(getPrice() <= 60_000) {
            throw new IncorrectCarPriceException("You entered wrong price. Business cars cost more than 60 000$");
        }
        if(getModel() != CarModel.BMW && getModel() != CarModel.AUDI && getModel() != CarModel.VOLKSWAGEN) {
            throw new IncorrectModelException("This car type can't be Business class.");
        }
    }

    @Override
    public void startTheCar() {
        System.out.println("Wroom-wroom-wroom!");
    }

    public Phone getPersonalPhone() {
        return personalPhone;
    }

    public String getPersonalDriverName() {
        return personalDriverName;
    }

    public void updateCar() {
        this.typeOfGlassProtection = ArmoredGlass.BULLETPROOF;
        this.setPrice(this.getPrice() + 40000);
    }

    public void changeEngine(Engine engine) {
        this.setMinSpeed(engine.getNumOfGears() * 4);
        this.setMaxSpeed(engine.getNumOfGears() / 2 + 78);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BusinessCar)) return false;
        return super.equals(o);
    }

    @Override
    public String toString() {
        return super.toString().replace("}", ", class = Business}");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
