package com.epam.learn.collections.main.taxi.types;

public enum CarModel {
    AUDI, BMW, HONDA, VOLKSWAGEN, RENAULT, LINCOLN, CHRYSLER
}
