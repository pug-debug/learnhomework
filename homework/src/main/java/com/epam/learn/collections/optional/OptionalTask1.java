package com.epam.learn.collections.optional;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class OptionalTask1 {
    public static void main(String[] args) throws IOException {
        List<String> poemLines = Files.readAllLines(
                Paths.get("src/main/java/com/epam/learn/collections/optional/Poem.txt"));
        Collections.reverse(poemLines);
        Files.write(Paths.get("src/main/java/com/epam/learn/collections/optional/ReversedPoem.txt"), poemLines);
    }
}
