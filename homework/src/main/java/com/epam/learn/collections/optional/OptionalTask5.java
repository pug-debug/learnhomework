package com.epam.learn.collections.optional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class OptionalTask5 {
    public static void main(String[] args) {
        System.out.println("Enter any amount of numbers. If you want to stop, enter any not empty symbol:");
        Scanner scanner = new Scanner(System.in);
        List<Integer> inputNumbers = new ArrayList<>();
        while (scanner.hasNextInt()) {
            inputNumbers.add(scanner.nextInt());
        }
        System.out.println(inputNumbers);
        Collections.sort(inputNumbers);
        Collections.reverse(inputNumbers);
        System.out.println(inputNumbers);
    }
}
