package com.epam.learn.collections.optional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class OptionalTask4 {
    public static void main(String[] args) throws IOException {
        List<String> poemLines = Files.readAllLines(Paths.get("src/main/java/com/epam/learn/collections/optional/Poem.txt"));
        poemLines.sort((s1, s2) -> s1.length() - s2.length());
        Files.write(Paths.get("src/main/java/com/epam/learn/collections/optional/PoemWithSortedLines.txt"), poemLines);
    }
}
