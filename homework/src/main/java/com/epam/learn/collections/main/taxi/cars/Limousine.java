package com.epam.learn.collections.main.taxi.cars;

import com.epam.learn.collections.main.taxi.carFeatures.TV;
import com.epam.learn.collections.main.taxi.exceptions.IncorrectCarPriceException;
import com.epam.learn.collections.main.taxi.exceptions.IncorrectModelException;
import com.epam.learn.collections.main.taxi.types.CarModel;

import java.util.HashSet;
import java.util.Set;

public class Limousine extends Car {
    private final String personalDriverName;
    private final TV tvModel;
    private Set<String> miniBarDrinks;

    public Limousine(CarModel model, int price, int fuelConsumption, int minSpeed, int maxSpeed,
                     TV tvModel, Set<String> miniBarDrinks, String personalDriverName) throws Exception {
        super(model, price, fuelConsumption, minSpeed, maxSpeed);
        this.personalDriverName = personalDriverName;
        this.tvModel = tvModel;
        this.miniBarDrinks = miniBarDrinks;
    }

    public String getPersonalDriverName() {
        return personalDriverName;
    }

    public TV getTvModel() {
        return tvModel;
    }

    public Set<String> getMiniBarDrinks() {
        return miniBarDrinks;
    }

    public void setMiniBarDrinks(Set<String> miniBarDrinks) {
        this.miniBarDrinks = miniBarDrinks;
    }

    @Override
    public void checkParameters() throws IncorrectCarPriceException, IncorrectModelException {
        if(getPrice() <= 75000) {
            throw new IncorrectCarPriceException("You entered wrong price. Limousines cost more than 75 000$");
        }
        if(getModel() != CarModel.LINCOLN && getModel() != CarModel.CHRYSLER) {
            throw new IncorrectModelException("This car type can't be limousine.");
        }
    }

    @Override
    public void startTheCar() {
        System.out.println("Wrrrrrrroooooooom! Let's have some fun!");
    }

    public void haveFun(Set<String> drinks) {
        Set<String> newSet = new HashSet<>(miniBarDrinks);
        newSet.removeAll(drinks);
        setMiniBarDrinks(newSet);
    }

    @Override
    public String toString() {
        return super.toString().replace("}", ", class = Limousine}");
    }
}
