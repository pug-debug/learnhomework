package com.epam.learn.collections.main.taxi.carFeatures;

public class Engine {
    private final int numOfGears;

    public Engine(int numOfGears) {
        this.numOfGears = numOfGears;
    }

    public int getNumOfGears() {
        return numOfGears;
    }
}
