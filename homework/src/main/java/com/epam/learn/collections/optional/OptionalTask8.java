package com.epam.learn.collections.optional;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.stream.Stream;

public class OptionalTask8 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = Files.newBufferedReader(Paths.get("src/main/java/com/epam/learn/collections/optional/TomSawyer.txt"));
        HashSet<String> wordsFromText = new HashSet<>();
                reader.lines().flatMap(w -> Stream.of(w.split("[^a-zA-Z0-9']+")))
                        .map(String::toLowerCase)
                        .forEach(wordsFromText::add);
        System.out.println(wordsFromText);
    }
}
