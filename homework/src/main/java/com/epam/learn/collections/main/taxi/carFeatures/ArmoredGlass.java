package com.epam.learn.collections.main.taxi.carFeatures;

public enum ArmoredGlass {
    VANDAL_PROOF, BURGLAR_PROOF, BULLETPROOF
}
