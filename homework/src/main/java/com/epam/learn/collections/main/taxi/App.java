package com.epam.learn.collections.main.taxi;

import com.epam.learn.collections.main.taxi.carFeatures.ArmoredGlass;
import com.epam.learn.collections.main.taxi.carFeatures.Phone;
import com.epam.learn.collections.main.taxi.carFeatures.TV;
import com.epam.learn.collections.main.taxi.cars.*;
import com.epam.learn.collections.main.taxi.types.CarModel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) throws Exception {
        List<Car> cars = Arrays.asList(
                new EconomyCar(CarModel.VOLKSWAGEN,1800, 50, 10, 200),
                new EconomyCar(CarModel.RENAULT,1400, 40, 5, 160),

                new BusinessCar(CarModel.AUDI,65000, 105, 40, 330, Phone.PHILIPS,
                        ArmoredGlass.BULLETPROOF, "Chris"),
                new BusinessCar(CarModel.BMW,78000, 110, 55, 380, Phone.ALCATEL,
                        ArmoredGlass.BURGLAR_PROOF, "Justin"),
                new BusinessCar(CarModel.VOLKSWAGEN,105000, 76, 34, 274,
                        Phone.ALCATEL, ArmoredGlass.VANDAL_PROOF, "George"),

                new Limousine(CarModel.LINCOLN, 78000, 100, 20, 160, TV.SAMSUNG,
                        Set.of("Vine", "Champagne", "Whiskey"), "Michael"),
                new Limousine(CarModel.CHRYSLER, 80000, 150, 25, 178, TV.TOSHIBA,
                        Set.of("Champagne", "Beer", "Vine"), "Lucas")
        );

        TaxiStation taxiStation = new TaxiStation(cars);
        System.out.printf("Price of Taxi Station: %d\n", taxiStation.getPriceOfAllCars());
        System.out.println("==========================================");
        System.out.println("Cars sorted by fuel consumption: \n" + taxiStation.sortCarsByFuelConsumption());
        System.out.println("==========================================");
        System.out.println("Get list of cars with speed in range 5-220: \n" + taxiStation.getCarsBySpeedRange(5, 220));
    }
}
