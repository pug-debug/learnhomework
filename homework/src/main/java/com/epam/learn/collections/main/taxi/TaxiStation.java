package com.epam.learn.collections.main.taxi;

import com.epam.learn.collections.main.taxi.cars.Car;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaxiStation {
    private final List<? extends Car> cars;

    public TaxiStation(List<? extends Car> cars) {
        this.cars = cars;
    }

    public int getPriceOfAllCars() {
        return cars.stream().mapToInt(Car::getPrice).sum();
    }

    public TaxiStation sortCarsByFuelConsumption() {
        cars.sort(Comparator.comparing(Car::getFuelConsumption));
        return this;
    }

    public List<Car> getCarsBySpeedRange(int minSpeed, int maxSpeed) {
         return cars.stream().filter(t -> t.getMinSpeed() >= minSpeed && t.getMaxSpeed() <= maxSpeed).
                 collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "TaxiStation{" +
                "cars=" + '\n' + cars.toString() +
                '}';
    }
}
