package com.epam.learn.collections.main.taxi.exceptions;

public class IncorrectCarPriceException extends Exception{
    public IncorrectCarPriceException(String message) {
        super(message);
    }
}
