package com.epam.learn.constants;

public class GoogleCloudXpaths {
    public final static String VM_INSTANCE_PRICING_XPATH = "//div[@class='gsc-result gs-webResult']/a[contains(text(), 'VM instance pricing')]";
    public final static String X_PATH_TO_CALCULATOR_PAGE = "//div[@class='gs-title']/descendant::*[text()='Google Cloud Pricing Calculator']";
    public final static String X_PATH_TO_CALCULATOR_PAGE_FROM_VM_PAGE = "//div[@class='cloud-section']/p/a[@class='cloud-link']";

}
