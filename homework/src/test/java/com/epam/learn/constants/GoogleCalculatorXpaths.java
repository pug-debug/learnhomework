package com.epam.learn.constants;

public class GoogleCalculatorXpaths {
    public static final String MACHINE_CLASSES_DROPDOWN = "//div[@id='select_container_98']/md-select-menu/md-content/md-option";
    public static final String SERIES_DROPDOWN =  "//div[@id='select_container_106']/md-select-menu/md-content/md-option";
    public static final String MACHINE_TYPE_DROPDOWN = "//div[@id='select_container_108']/md-select-menu/md-content/md-optgroup/md-option";
    public static final String GPU_TYPE_DROPDOWN = "//div[@id='select_container_457']/md-select-menu/md-content/md-option";
    public static final String GPU_NUMBER_DROPDOWN = "//div[@id='select_container_459']/md-select-menu/md-content/md-option";
    public static final String LOCAL_SSD_DROPDOWN = "//div[@id='select_container_419']/md-select-menu/md-content/md-option";
    public static final String DATACENTER_LOCATION_DROPDOWN = "//div[@id='select_container_114']/md-select-menu/md-content/md-optgroup/md-option";
    public static final String COMMITTED_USAGE_DROPDOWN = "//div[@id='select_container_121']/md-select-menu/md-content/md-option";
}
