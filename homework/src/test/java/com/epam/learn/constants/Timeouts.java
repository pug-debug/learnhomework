package com.epam.learn.constants;

public class Timeouts {
    public final static int GENERAL_TIME_OUT_SECONDS = 40;
    public final static int EXPLICIT_WAIT_TIME_OUT_SECONDS = 70;
    public final static int IMPLICIT_WAIT_TIME_OUT_SECONDS = 50;
    public final static int CAPTCHA_TIMEOUT_SECONDS = 55;
    public final static int VM_INSTANCE_PAGE_TIMEOUT_SECONDS = 15;
}
