package com.epam.learn.constants;

public class PasteBinXpaths {
    public final static String PASTE_EXPIRATION_DROPDOWN = "//ul[@class='select2-results__options']/li";
    public final static String SYNTAX_HIGHLIGHTING_DROPDOWN = "//ul[@class='select2-results__options select2-results__options--nested']/li";
}
