package com.epam.learn.googleCalculator.pages.calculatorPages;

import com.epam.learn.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleCloudPricingCalculatorResultPageWithEmailForm extends BasePage {

    @FindBy(xpath = "//*[@id='cloud-site']/devsite-iframe/iframe")
    private WebElement majorFrame;

    @FindBy(css = "#myFrame")
    private WebElement mainFrame;

    @FindBy(xpath = "//input[@name='description' and @type='email']")
    private WebElement fieldToEnterEmail;

    @FindBy(xpath = "//md-dialog-actions/button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']")
    private WebElement sendEmailButton;

    public GoogleCloudPricingCalculatorResultPageWithEmailForm(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterEmailAddress(String emailAddress) {
        driver.switchTo().frame(majorFrame);
        driver.switchTo().frame(mainFrame);
        fieldToEnterEmail.sendKeys(emailAddress);
        logger.info("enter email address: " + emailAddress);
    }

    public void sendEmail() {
        scrollToElement(fieldToEnterEmail);
        waitUntilElementIsClickable(sendEmailButton);
        logger.info("sending email");
        sendEmailButton.click();
    }
}