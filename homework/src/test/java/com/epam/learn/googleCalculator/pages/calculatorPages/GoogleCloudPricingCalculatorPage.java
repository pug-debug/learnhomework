package com.epam.learn.googleCalculator.pages.calculatorPages;

import com.epam.learn.constants.GoogleCalculatorXpaths;
import com.epam.learn.BasePage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class GoogleCloudPricingCalculatorPage extends BasePage {

    @FindBy(xpath = "//*[@id='cloud-site']/devsite-iframe/iframe")
    private WebElement majorFrame;

    @FindBy(css = "#myFrame")
    private WebElement mainFrame;

    @FindBy(xpath = "//div[@title='Compute Engine']")
    private WebElement computeEngineButton;

    @FindBy(xpath = "//input[@type='number' and @name='quantity']")
    private WebElement numberOfInstances;

    @FindBy(xpath = "//md-select[@placeholder='VM Class']/md-select-value")
    private WebElement machineClass;

    @FindBy(xpath = "//md-select[contains(@aria-label, 'Operating System')]/md-select-value/span[@class='md-select-icon']")
    private WebElement operatingSystemSelect;


    @FindBy(xpath = "//md-select[@placeholder='Series']/md-select-value")
    private WebElement seriesChoice;

    @FindBy(xpath = "//md-select[contains(@aria-label, 'Instance type')]/md-select-value/span[@class='md-select-icon']")
    private WebElement machineTypeChoice;

    @FindBy(xpath = "//md-checkbox/div[contains(text(), 'Add GPUs.')]")
    private WebElement addGPUCheckbox;

    @FindBy(xpath = "//md-select[@placeholder='GPU type']")
    private WebElement gpuTypeChoice;

    @FindBy(xpath = "//md-select[@placeholder='Number of GPUs']/md-select-value/span[@class='md-select-icon']")
    private WebElement numberOfGPU;

    @FindBy(xpath = "//md-select[@placeholder='Local SSD']/md-select-value/span[@class='md-select-icon']")
    private WebElement localSSD;

    @FindBy(xpath = "//md-select[@placeholder='Datacenter location']/md-select-value/span[@class='md-select-icon']")
    private WebElement datacenterLocation;

    @FindBy(xpath = "//md-select[@placeholder='Committed usage']/md-select-value")
    private WebElement committedUsage;

    @FindBy(xpath = "//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']")
    private List<WebElement> addToEstimateButtons;

    public GoogleCloudPricingCalculatorPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void chooseComputeEngine() {
        waitUntilPageIsLoaded();
        setImplicitWait();
        logger.info("switch to major Frame");
        driver.switchTo().frame(majorFrame);
        logger.info("switch to main Frame");
        waitUntilFrameToBeAvailableAndSwitchToIt(mainFrame);
        logger.info("choose Compute engine");
        setImplicitWait();
        computeEngineButton.click();
    }

    public void setNumberOfInstances(String numOfInstances) {
        setImplicitWait();
        numberOfInstances.sendKeys(numOfInstances);
        logger.info("set number of instances: " + numOfInstances);
    }

    public void chooseOperatingSystem(String operatingSystem) {
        logger.info("choose operating system: " + operatingSystem);
        waitUntilElementIsClickable(operatingSystemSelect);
        operatingSystemSelect.click();
        setImplicitWait();
        WebElement operationSystemOption = driver.findElement(By.xpath
                (String.format("//md-option[@class='md-ink-ripple']/div[contains(text(), '%s')]", operatingSystem)));
        waitUntilElementIsClickable(operationSystemOption);
        operationSystemOption.click();
        logger.info("chosen OS: " + operationSystemOption.getAttribute("innerText"));
    }

    public void selectMachineClass(String machineClassOption) {
        logger.info("set machine class: " + machineClassOption);
        waitUntilElementIsClickable(machineClass);
        machineClass.click();
        setImplicitWait();
        List<WebElement> machineClasses = driver.findElements(
                By.xpath(GoogleCalculatorXpaths.MACHINE_CLASSES_DROPDOWN));
        WebElement currentMachineClass = getNecessaryOptionFromDropbox(machineClasses, machineClassOption);
        waitUntilElementIsClickable(currentMachineClass);
        currentMachineClass.click();
        logger.info("machine class chosen: " + currentMachineClass.getAttribute("innerText"));
    }

    public void chooseSeries(String series) {
        logger.info("choose series: " + series);
        scrollToElement(operatingSystemSelect);
        waitUntilElementIsClickable(seriesChoice);
        seriesChoice.click();
        setImplicitWait();
        List<WebElement> seriesOptions = driver.findElements(By.xpath(GoogleCalculatorXpaths.SERIES_DROPDOWN));
        setImplicitWait();
        WebElement currentSeriesType = getNecessaryOptionFromDropbox(seriesOptions, series);
        waitUntilElementIsClickable(currentSeriesType);
        currentSeriesType.click();
        logger.info("chosen series: " + currentSeriesType.getAttribute("innerText"));
    }

    public void chooseMachineType(String machineType) {
        logger.info("choose machine type: " + machineType);
        scrollToElement(numberOfInstances);
        waitUntilElementIsClickable(machineTypeChoice);
        machineTypeChoice.click();
        setImplicitWait();
        List<WebElement> machineTypesAll = driver.findElements(By.xpath(GoogleCalculatorXpaths.MACHINE_TYPE_DROPDOWN));
        WebElement currentMachineType = getNecessaryOptionFromDropbox(machineTypesAll, machineType);
        currentMachineType.click();
        logger.info("chosen machine type: " + currentMachineType.getAttribute("innerText"));
    }

    public void addGPU() {
        waitUntilElementIsClickable(addGPUCheckbox);
        addGPUCheckbox.click();
        logger.info("add GPU");
        setImplicitWait();
    }

    public void selectGPUType(String GPUType) {
        logger.info("select GPU type: " + GPUType);
        scrollToElement(machineTypeChoice);
        waitUntilElementIsClickable(gpuTypeChoice);
        gpuTypeChoice.click();
        setImplicitWait();
        List<WebElement> gpuTypes = driver.findElements(By.xpath(GoogleCalculatorXpaths.GPU_TYPE_DROPDOWN));
        WebElement currentGPUType = getNecessaryOptionFromDropbox(gpuTypes, GPUType);
        waitUntilElementIsClickable(currentGPUType);
        currentGPUType.click();
        logger.info("chosen GPU Type: " + currentGPUType.getAttribute("innerText"));
    }

    public void selectNumberOfGPU(String numOfGPU) {
        logger.info("select number of GPU: " + numOfGPU);
        waitUntilElementIsClickable(numberOfGPU);
        numberOfGPU.click();
        setImplicitWait();
        List<WebElement> numberOfGPUOptions = driver.findElements(By.xpath(GoogleCalculatorXpaths.GPU_NUMBER_DROPDOWN));
        WebElement currentNumberOfGPU = getNecessaryOptionFromDropbox(numberOfGPUOptions, numOfGPU);
        waitUntilElementIsClickable(currentNumberOfGPU);
        currentNumberOfGPU.click();
        logger.info("chosen number of GPU: " + currentNumberOfGPU.getAttribute("innerText"));
        scrollToElement(datacenterLocation);
        setImplicitWait();
    }

    public void selectLocalSSD(String localSSDInfo) {
        logger.info("select local SSD: " + localSSDInfo);
        scrollToElement(gpuTypeChoice);
        waitUntilElementIsClickable(localSSD);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        try {
            localSSD.click();
        } catch (ElementClickInterceptedException e) {
            executor.executeScript("arguments[0].click();", localSSD);
        }
        setImplicitWait();
        List<WebElement> localSSDOptions = driver.findElements(By.xpath(GoogleCalculatorXpaths.LOCAL_SSD_DROPDOWN));
        setImplicitWait();
        WebElement chosenLocalSSD = getNecessaryOptionFromDropbox(localSSDOptions, localSSDInfo);
        try {
            chosenLocalSSD.click();
        } catch (ElementClickInterceptedException e) {
            executor.executeScript("arguments[0].click();", chosenLocalSSD);
        }
        logger.info("chosen local SSD: " + chosenLocalSSD.getAttribute("innerText"));
    }

    public void selectDatacenterLocation(String datacenterLocationToChoose) {
        logger.info("select datacenter location: " + datacenterLocationToChoose);
        waitUntilElementIsClickable(datacenterLocation);
        datacenterLocation.click();
        setImplicitWait();
        List<WebElement> datacenterLocations = driver.findElements(By.xpath(GoogleCalculatorXpaths.DATACENTER_LOCATION_DROPDOWN));
        WebElement currentDatacenterLocation = getNecessaryOptionFromDropbox(datacenterLocations, datacenterLocationToChoose);
        waitUntilElementIsClickable(currentDatacenterLocation);
        currentDatacenterLocation.click();
        logger.info("chosen datacenter location: " + currentDatacenterLocation.getAttribute("innerText"));
    }

    public void setCommittedUsage(String usageTerm) {
        logger.info("select usage term: " + usageTerm);
        waitUntilElementIsClickable(committedUsage);
        committedUsage.click();
        setImplicitWait();
        List<WebElement> usageOptions = driver.findElements(By.xpath(GoogleCalculatorXpaths.COMMITTED_USAGE_DROPDOWN));
        WebElement currentUsageTerm = getNecessaryOptionFromDropbox(usageOptions, usageTerm);
        waitUntilElementIsClickable(currentUsageTerm);
        currentUsageTerm.click();
        logger.info("chosen usage term: " + currentUsageTerm.getAttribute("innerText"));
    }

    public GoogleCloudPricingCalculatorResultPage clickAddToEstimateButton() {
        waitUntilElementIsClickable(addToEstimateButtons.get(0));
        addToEstimateButtons.get(0).click();
        logger.info("created new calculation and push Add to estimate button");
        return new GoogleCloudPricingCalculatorResultPage(driver);
    }
}