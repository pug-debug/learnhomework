package com.epam.learn.googleCalculator.exceptions;

import org.openqa.selenium.WebDriverException;

public class GoogleCalculatorException extends WebDriverException {
    public GoogleCalculatorException(String message) {
        super(message);
    }
}
