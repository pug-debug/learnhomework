package com.epam.learn.googleCalculator.pages.googleCloudPages;

import com.epam.learn.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CloudGoogleComPage extends BasePage {
    private static final String HOME_PAGE = "https://cloud.google.com";

    @FindBy(xpath = "//*[@name='q' and @type='text']")
    private WebElement searchInput;

    public CloudGoogleComPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public CloudGoogleComPage openPage() {
        driver.get(HOME_PAGE);
        waitUntilVisibilityOfElement(searchInput);
        return this;
    }

    public CloudGoogleSearchResultPage executeSearchQuery(String searchQuery) {
        searchInput.click();
        searchInput.sendKeys(searchQuery);
        searchInput.sendKeys(Keys.ENTER);
        logger.info("search query completed: " + searchQuery);
        return new CloudGoogleSearchResultPage(driver);
    }
}