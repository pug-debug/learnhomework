package com.epam.learn.googleCalculator.stepClasses;

import com.epam.learn.googleCalculator.model.Calculator;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorPage;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorResultPage;

public class CalculationSteps {
    public  GoogleCloudPricingCalculatorPage calculatorPage;
    public CalculationSteps(GoogleCloudPricingCalculatorPage calculatorPage) {
        this.calculatorPage = calculatorPage;
    }


    public GoogleCloudPricingCalculatorResultPage makeCalculations(Calculator calculator) {
        calculatorPage.chooseComputeEngine();
        calculatorPage.setNumberOfInstances(calculator.getNumberOfInstances());
        calculatorPage.chooseOperatingSystem(calculator.getOperatingSystem());
        calculatorPage.selectMachineClass(calculator.getMachineClass());
        calculatorPage.chooseSeries(calculator.getSeries());
        calculatorPage.chooseMachineType(calculator.getInstanceType());
        calculatorPage.addGPU();
        calculatorPage.selectGPUType(calculator.getGpu());
        calculatorPage.selectNumberOfGPU(calculator.getNumberOfGPU());
        calculatorPage.selectLocalSSD(calculator.getLocalSSDInfo());
        calculatorPage.selectDatacenterLocation(calculator.getDatacenterLocation());
        calculatorPage.setCommittedUsage(calculator.getUsageTerm());
        return calculatorPage.clickAddToEstimateButton();
    }
}