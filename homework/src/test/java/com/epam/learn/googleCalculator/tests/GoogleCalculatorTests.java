package com.epam.learn.googleCalculator.tests;

import com.epam.learn.googleCalculator.model.Calculator;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorPage;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorResultPage;
import com.epam.learn.googleCalculator.pages.googleCloudPages.CloudGoogleSearchResultPage;
import com.epam.learn.googleCalculator.pages.yopmailPages.YopmailWithRandomEmailAddressPage;
import com.epam.learn.googleCalculator.service.CalculatorCreator;
import com.epam.learn.googleCalculator.service.CalculationValidation;
import com.epam.learn.googleCalculator.stepClasses.CalculationSteps;
import com.epam.learn.googleCalculator.stepClasses.EmailSteps;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleCalculatorTests extends CommonConditions {
    Calculator calculator = CalculatorCreator.createNewCalculator();

    @Test(description = "Hurt Me Plenty")
    public void calculatorTest() {
        CloudGoogleSearchResultPage resultPage = googleCloudSteps.executeSearchQuery(calculator.getSearchQuery());
        GoogleCloudPricingCalculatorPage calculatorPage = googleCloudSteps.openCalculatorPage(resultPage);
        GoogleCloudPricingCalculatorResultPage calculatorResultPage = new CalculationSteps(calculatorPage)
                .makeCalculations(calculator);
        Assert.assertTrue(CalculationValidation.checkCalculationResults(calculatorResultPage, calculator));
    }

    @Test(description = "Hardcore")
    public void sendCalculationResultsOnEmailTest() {
        CloudGoogleSearchResultPage resultPage = googleCloudSteps.executeSearchQuery(calculator.getSearchQuery());
        GoogleCloudPricingCalculatorPage calculatorPage = googleCloudSteps.openCalculatorPage(resultPage);
        GoogleCloudPricingCalculatorResultPage calculatorResultPage = new CalculationSteps(calculatorPage)
                .makeCalculations(calculator);
        String estimatedCost = calculatorResultPage.getTotalEstimatedCost();

        EmailSteps emailSteps = new EmailSteps(driver);
        YopmailWithRandomEmailAddressPage yopmailWithRandomEmailAddressPage = emailSteps.sendEmail(calculatorResultPage);
        String billInfo = emailSteps.checkEmail(yopmailWithRandomEmailAddressPage);

        Assert.assertTrue(estimatedCost.contains(billInfo));
    }
}