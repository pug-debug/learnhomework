package com.epam.learn.googleCalculator.pages.yopmailPages;

import com.epam.learn.googleCalculator.exceptions.GoogleCalculatorException;
import com.epam.learn.BasePage;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Arrays;
import java.util.stream.Collectors;

public class YopmailPageWithLetter extends BasePage {

    @FindBy(id = "ifmail")
    private WebElement frame;

    @FindBy(xpath = "//iframe[@title='reCAPTCHA']")
    private WebElement captchaFrame;

    @FindBy(id = "recaptcha-anchor")
    private WebElement captchaCheckbox;

    @FindBy(xpath = "//td/h2")
    private WebElement stringWithCost;

    @FindBy(css = "#refresh")
    private WebElement refreshButton;

    public YopmailPageWithLetter(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getBillInformation() {
        waitUntilElementIsClickable(refreshButton);
        logger.info("click refresh button");
        String billInformation = "";
        logger.info("checking estimated cost in email");
        for(int i = 0; i < 5; i++) {
            try {
                driver.switchTo().defaultContent();
                refreshButton.click();
                waitUntilFrameToBeAvailableAndSwitchToIt(frame);
                billInformation = Arrays.stream(stringWithCost.getAttribute("innerText").split("USD "))
                        .collect(Collectors.toList()).get(1);
                break;
            } catch (NoSuchWindowException | NoSuchElementException e) {
                if(i == 4) {
                    throw new GoogleCalculatorException("Limit of attempts to get email information.");
                }
            } catch (ElementClickInterceptedException e) {
                waitUntilFrameToBeAvailableAndSwitchToIt(captchaFrame);
                waitUntilElementIsClickable(captchaCheckbox);
                captchaCheckbox.click();
            }
        }
        logger.info("cost: " + billInformation);
        return billInformation;
    }
}