package com.epam.learn.googleCalculator.service;

import com.epam.learn.googleCalculator.model.Calculator;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorResultPage;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CalculationValidation {
    public static final String EXPECTED_COST = "testdata.expectedCost";

    public static boolean checkCalculationResults(GoogleCloudPricingCalculatorResultPage resultPage, Calculator calculator) {
        String expectedCost = TestDataReader.getTestData(EXPECTED_COST);
        return resultPage.getRegionInfo().contains(calculator.getDatacenterLocation())
                && resultPage.getMachineClassInfo().contains(calculator.getMachineClass().toLowerCase())
                && resultPage.getCommitmentTermInfo().contains(calculator.getUsageTerm())
                && resultPage.getInstanceTypeInfo().contains(Arrays.stream(calculator.getInstanceType().split(" "))
                .collect(Collectors.toList()).get(0))
                && resultPage.getLocalSSDInfo().contains(calculator.getLocalSSDInfo())
                && resultPage.getTotalEstimatedCost().contains(expectedCost);
    }
}
