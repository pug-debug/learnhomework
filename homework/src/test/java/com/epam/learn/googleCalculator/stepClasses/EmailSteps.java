package com.epam.learn.googleCalculator.stepClasses;

import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorResultPage;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorResultPageWithEmailForm;
import com.epam.learn.googleCalculator.pages.yopmailPages.YopmailPage;
import com.epam.learn.googleCalculator.pages.yopmailPages.YopmailPageWithLetter;
import com.epam.learn.googleCalculator.pages.yopmailPages.YopmailWithRandomEmailAddressPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

public class EmailSteps {
    private final WebDriver driver;

    public EmailSteps(WebDriver driver) {
        this.driver = driver;
    }

    public YopmailWithRandomEmailAddressPage sendEmail(GoogleCloudPricingCalculatorResultPage calculatorResultPage) {
        GoogleCloudPricingCalculatorResultPageWithEmailForm pageWithEmailForm = calculatorResultPage.estimateEmail();
        String calculatorWindow = driver.getWindowHandle();
        driver.switchTo().newWindow(WindowType.TAB);
        YopmailWithRandomEmailAddressPage yopmailWithRandomEmailAddressPage = new YopmailPage(driver).openPage()
                .getRandomEmailAddress();
        String yopMailWindow = driver.getWindowHandle();
        String randomEmail = yopmailWithRandomEmailAddressPage.copyEmailAddress();
        driver.switchTo().window(calculatorWindow);
        pageWithEmailForm.enterEmailAddress(randomEmail);
        pageWithEmailForm.sendEmail();
        driver.switchTo().window(yopMailWindow);
        return yopmailWithRandomEmailAddressPage;
    }

    public String checkEmail(YopmailWithRandomEmailAddressPage yopmailWithRandomEmailAddressPage) {
        YopmailPageWithLetter pageWithLetter = yopmailWithRandomEmailAddressPage.checkEmail();
        return pageWithLetter.getBillInformation();
    }
}
