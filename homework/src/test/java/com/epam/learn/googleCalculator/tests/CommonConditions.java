package com.epam.learn.googleCalculator.tests;

import com.epam.learn.googleCalculator.driver.DriverSingleton;
import com.epam.learn.googleCalculator.stepClasses.GoogleCloudSteps;
import com.epam.learn.googleCalculator.util.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class CommonConditions {
    protected WebDriver driver;
    protected GoogleCloudSteps googleCloudSteps;


    @BeforeMethod(alwaysRun = true)
    public void browserSetup() {
        driver = DriverSingleton.getDriver();
        googleCloudSteps = new GoogleCloudSteps(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void closeBrowser() {
        DriverSingleton.closeDriver();
    }
}
