package com.epam.learn.googleCalculator.pages.yopmailPages;

import com.epam.learn.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YopmailWithRandomEmailAddressPage extends BasePage {

    @FindBy(xpath = "//div[@id='egen']")
    private WebElement fieldWithGeneratedEmailAddress;

    @FindBy(xpath = "//span[contains(text(),'Проверить почту')]")
    private WebElement checkEmailButton;

    public YopmailWithRandomEmailAddressPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String copyEmailAddress() {
        return fieldWithGeneratedEmailAddress.getAttribute("innerText");
    }

    public YopmailPageWithLetter checkEmail() {
        checkEmailButton.click();
        waitUntilPageIsLoaded();
        return new YopmailPageWithLetter(driver);
    }
}