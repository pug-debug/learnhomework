package com.epam.learn.googleCalculator.service;

import com.epam.learn.googleCalculator.model.Calculator;

public class CalculatorCreator {
    public static final String TESTDATA_SEARCH_QUERY = "testdata.searchQuery";
    public static final String TESTDATA_NUMBER_OF_INSTANCES = "testdata.numberOfInstances";
    public static final String TESTDATA_MACHINE_CLASS = "testdata.machineClass";
    public static final String TESTDATA_OPERATING_SYSTEM = "testdata.operatingSystem";
    public static final String TESTDATA_SERIES = "testdata.series";
    public static final String TESTDATA_INSTANCE_TYPE = "testdata.instanceType";
    public static final String TESTDATA_NUMBER_OF_GPU = "testdata.numberOfGpu";
    public static final String TESTDATA_GPU = "testdata.gpu";
    public static final String TESTDATA_LOCAL_SSD_INFO = "testdata.localSSDInfo";
    public static final String TESTDATA_DATACENTER_LOCATION = "testdata.datacenterLocation";
    public static final String TESTDATA_USAGE_TERM = "testdata.usageTerm";


    public static Calculator createNewCalculator() {
        return new Calculator(TestDataReader.getTestData(TESTDATA_SEARCH_QUERY),
                TestDataReader.getTestData(TESTDATA_NUMBER_OF_INSTANCES), TestDataReader.getTestData(TESTDATA_MACHINE_CLASS),
                TestDataReader.getTestData(TESTDATA_OPERATING_SYSTEM), TestDataReader.getTestData(TESTDATA_SERIES),
                TestDataReader.getTestData(TESTDATA_INSTANCE_TYPE), TestDataReader.getTestData(TESTDATA_NUMBER_OF_GPU),
                TestDataReader.getTestData(TESTDATA_GPU), TestDataReader.getTestData(TESTDATA_LOCAL_SSD_INFO),
                TestDataReader.getTestData(TESTDATA_DATACENTER_LOCATION), TestDataReader.getTestData(TESTDATA_USAGE_TERM));
    }
}
