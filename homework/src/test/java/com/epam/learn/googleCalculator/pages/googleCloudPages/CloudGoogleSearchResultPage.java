package com.epam.learn.googleCalculator.pages.googleCloudPages;

import com.epam.learn.constants.GoogleCloudXpaths;
import com.epam.learn.BasePage;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorPage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CloudGoogleSearchResultPage extends BasePage {

    @FindBy(xpath = "//div[@class='gs-title']/descendant::*[text()='Google Cloud Pricing Calculator']")
    private WebElement linkToCalculatorPage;

    public CloudGoogleSearchResultPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public GoogleCloudPricingCalculatorPage goToCalculatorPage() {
        try {
            waitUntilPresenceOfElement(GoogleCloudXpaths.X_PATH_TO_CALCULATOR_PAGE);
            linkToCalculatorPage.click();
            logger.info("redirecting to calculator page");
        } catch (TimeoutException e) {
            WMInstancePricingPage wmInstancePricingPage = openWMInstancePricingPage();
            wmInstancePricingPage.goToCalculatorPage();
            logger.info("redirecting to VM instance page");
        }
        return new GoogleCloudPricingCalculatorPage(driver);
    }

    private WMInstancePricingPage openWMInstancePricingPage() {
        WebElement wmInstancePricing = driver.findElement(By.xpath(GoogleCloudXpaths.VM_INSTANCE_PRICING_XPATH));
        wmInstancePricing.click();
        return new WMInstancePricingPage(driver);
    }
}