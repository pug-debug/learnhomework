package com.epam.learn.googleCalculator.model;

import java.util.Objects;

public class Calculator {
    private String searchQuery;
    private String numberOfInstances;
    private String machineClass;
    private String operatingSystem;
    private String series;
    private String instanceType;
    private String numberOfGPU;
    private String gpu;
    private String localSSDInfo;
    private String datacenterLocation;
    private String usageTerm;

    public Calculator(String searchQuery, String numberOfInstances, String machineClass, String operatingSystem,
                      String series, String instanceType, String numberOfGPU, String gpu, String localSSDInfo,
                      String datacenterLocation, String usageTerm) {
        this.searchQuery = searchQuery;
        this.numberOfInstances = numberOfInstances;
        this.machineClass = machineClass;
        this.operatingSystem = operatingSystem;
        this.series = series;
        this.instanceType = instanceType;
        this.numberOfGPU = numberOfGPU;
        this.gpu = gpu;
        this.localSSDInfo = localSSDInfo;
        this.datacenterLocation = datacenterLocation;
        this.usageTerm = usageTerm;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public String getMachineClass() {
        return machineClass;
    }

    public void setMachineClass(String machineClass) {
        this.machineClass = machineClass;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public String getNumberOfGPU() {
        return numberOfGPU;
    }

    public void setNumberOfGPU(String numberOfGPU) {
        this.numberOfGPU = numberOfGPU;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public String getLocalSSDInfo() {
        return localSSDInfo;
    }

    public void setLocalSSDInfo(String localSSDInfo) {
        this.localSSDInfo = localSSDInfo;
    }

    public String getDatacenterLocation() {
        return datacenterLocation;
    }

    public void setDatacenterLocation(String datacenterLocation) {
        this.datacenterLocation = datacenterLocation;
    }

    public String getUsageTerm() {
        return usageTerm;
    }

    public void setUsageTerm(String usageTerm) {
        this.usageTerm = usageTerm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Calculator that = (Calculator) o;
        return numberOfInstances == that.numberOfInstances && numberOfGPU == that.numberOfGPU
                && Objects.equals(searchQuery, that.searchQuery) && Objects.equals(machineClass, that.machineClass)
                && Objects.equals(operatingSystem, that.operatingSystem) && Objects.equals(series, that.series)
                && Objects.equals(instanceType, that.instanceType) && Objects.equals(gpu, that.gpu)
                && Objects.equals(localSSDInfo, that.localSSDInfo) && Objects.equals(datacenterLocation, that.datacenterLocation)
                && Objects.equals(usageTerm, that.usageTerm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSearchQuery(), getNumberOfInstances(), getMachineClass(), getOperatingSystem(),getSeries(),
                getInstanceType(), getNumberOfGPU(), getGpu(), getLocalSSDInfo(), getDatacenterLocation(), getUsageTerm());
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "searchQuery='" + searchQuery + '\'' +
                ", numberOfInstances=" + numberOfInstances +
                ", machineClass='" + machineClass + '\'' +
                ", operatingSystem='" + operatingSystem + '\'' +
                ", series='" + series + '\'' +
                ", instanceType='" + instanceType + '\'' +
                ", numberOfGPU=" + numberOfGPU +
                ", gpu='" + gpu + '\'' +
                ", localSSDInfo='" + localSSDInfo + '\'' +
                ", datacenterLocation='" + datacenterLocation + '\'' +
                ", usageTerm='" + usageTerm + '\'' +
                '}';
    }
}