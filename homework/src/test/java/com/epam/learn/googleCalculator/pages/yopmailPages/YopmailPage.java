package com.epam.learn.googleCalculator.pages.yopmailPages;

import com.epam.learn.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YopmailPage extends BasePage {
    private static final String HOME_PAGE = "https://yopmail.com/ru/";

    @FindBy(xpath = "//a[@href='email-generator']/div[@class='icolien']")
    private WebElement getRandomEmailAddressButton;

    public YopmailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public YopmailPage openPage() {
        driver.get(HOME_PAGE);
        waitUntilPageIsLoaded();
        return this;
    }

    public YopmailWithRandomEmailAddressPage getRandomEmailAddress() {
        getRandomEmailAddressButton.click();
        waitUntilPageIsLoaded();
        logger.info("getting random email address");
        return new YopmailWithRandomEmailAddressPage(driver);
    }
}