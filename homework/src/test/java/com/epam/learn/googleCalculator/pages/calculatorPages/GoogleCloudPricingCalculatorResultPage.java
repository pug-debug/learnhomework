package com.epam.learn.googleCalculator.pages.calculatorPages;

import com.epam.learn.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleCloudPricingCalculatorResultPage extends BasePage {

    @FindBy(xpath = "//div[@class='md-list-item-text ng-binding' and contains(text(),'Region')]")
    private WebElement region;

    @FindBy(xpath = "//div[@class='md-list-item-text ng-binding' and contains(text(),'Commitment')]")
    private WebElement commitmentTerm;

    @FindBy(xpath = "//div[@class='md-list-item-text ng-binding' and contains(text(),'VM class')]")
    private WebElement machineClass;

    @FindBy(xpath = "//div[@class='md-list-item-text ng-binding cpc-cart-multiline flex' and contains(text(),'Instance')]")
    private WebElement instanceType;

    @FindBy(xpath = "//div[@class='md-list-item-text ng-binding cpc-cart-multiline flex' and contains(text(),'Local SSD')]")
    private WebElement localSSD;

    @FindBy(xpath = "//h2[@class='md-title']/b")
    private WebElement totalEstimatedCost;

    @FindBy(xpath = "//button[@id='email_quote']")
    private WebElement emailEstimateButton;

    public GoogleCloudPricingCalculatorResultPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        waitUntilPageIsLoaded();
    }

    public GoogleCloudPricingCalculatorResultPageWithEmailForm estimateEmail() {
        scrollToElement(region);
        emailEstimateButton.click();
        waitUntilPageIsLoaded();
        return new GoogleCloudPricingCalculatorResultPageWithEmailForm(driver);
    }

    public String getTotalEstimatedCost() {
        return totalEstimatedCost.getAttribute("innerText");
    }

    public String getRegionInfo() {
        return region.getAttribute("innerText");
    }

    public String getCommitmentTermInfo() {
        return commitmentTerm.getAttribute("innerText");
    }

    public String getMachineClassInfo() {
        return machineClass.getAttribute("innerText");
    }

    public String getInstanceTypeInfo() {
        return instanceType.getAttribute("innerText");
    }

    public String getLocalSSDInfo() {
        return localSSD.getAttribute("innerText");
    }
}