package com.epam.learn.googleCalculator.stepClasses;

import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorPage;
import com.epam.learn.googleCalculator.pages.googleCloudPages.CloudGoogleComPage;
import com.epam.learn.googleCalculator.pages.googleCloudPages.CloudGoogleSearchResultPage;
import org.openqa.selenium.WebDriver;

public class GoogleCloudSteps {
    public WebDriver driver;

    public GoogleCloudSteps(WebDriver driver) {
        this.driver = driver;
    }

    public CloudGoogleSearchResultPage executeSearchQuery(String searchQuery) {
        return new CloudGoogleComPage(driver).openPage().executeSearchQuery(searchQuery);
    }
    public GoogleCloudPricingCalculatorPage openCalculatorPage(CloudGoogleSearchResultPage resultPage) {
        return resultPage.goToCalculatorPage();
    }
}
