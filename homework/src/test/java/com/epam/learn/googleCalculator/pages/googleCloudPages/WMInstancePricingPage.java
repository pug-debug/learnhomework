package com.epam.learn.googleCalculator.pages.googleCloudPages;

import com.epam.learn.constants.GoogleCloudXpaths;
import com.epam.learn.BasePage;
import com.epam.learn.googleCalculator.pages.calculatorPages.GoogleCloudPricingCalculatorPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WMInstancePricingPage extends BasePage {

    @FindBy(xpath = "//div[@class='cloud-section']/ul/li/a[@href='/products/calculator']")
    private WebElement linkToCalculatorPage;

    public WMInstancePricingPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public GoogleCloudPricingCalculatorPage goToCalculatorPage() {
        scrollToElement(GoogleCloudXpaths.X_PATH_TO_CALCULATOR_PAGE_FROM_VM_PAGE);
        linkToCalculatorPage.click();
        logger.info("redirecting to calculator page");
        return new GoogleCloudPricingCalculatorPage(driver);
    }
}