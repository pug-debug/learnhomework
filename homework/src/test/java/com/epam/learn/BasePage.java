package com.epam.learn;

import com.epam.learn.constants.Timeouts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import java.util.stream.Collectors;

public abstract class BasePage {
    public WebDriver driver;
    public final Logger logger = LogManager.getRootLogger();

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void scrollToElement(String xpath) {
        WebElement elementToScroll = driver.findElement(By.xpath(xpath));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elementToScroll);
    }

    public void scrollToElement(WebElement elementToScroll) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elementToScroll);
    }

    public void waitUntilVisibilityOfElement(WebElement elementToBeVisible) {
        new WebDriverWait(driver, Duration.ofSeconds(Timeouts.EXPLICIT_WAIT_TIME_OUT_SECONDS))
                .until(ExpectedConditions.visibilityOf(elementToBeVisible));
    }

    public void waitUntilPresenceOfElement(String xpathToElement) {
        new WebDriverWait(driver, Duration.ofSeconds(Timeouts.EXPLICIT_WAIT_TIME_OUT_SECONDS))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathToElement)));
    }

    public void waitUntilElementIsClickable(WebElement elementToBeClickable) {
        new WebDriverWait(driver, Duration.ofSeconds(Timeouts.EXPLICIT_WAIT_TIME_OUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(elementToBeClickable));
    }

    public void waitUntilFrameToBeAvailableAndSwitchToIt(WebElement frameToSwitchTo) {
        new WebDriverWait(driver, Duration.ofSeconds(Timeouts.CAPTCHA_TIMEOUT_SECONDS))
                .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameToSwitchTo));
    }

    public void waitUntilPageIsLoaded() {
        new WebDriverWait(driver, Duration.ofSeconds(Timeouts.GENERAL_TIME_OUT_SECONDS)).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState")
                        .equals("complete"));
    }

    public void setImplicitWait() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(Timeouts.IMPLICIT_WAIT_TIME_OUT_SECONDS));
    }

    public WebElement getNecessaryOptionFromDropbox(List<WebElement> options, String optionToChoose) {
        return options.stream()
                .filter(p-> p.getAttribute("innerText").contains(optionToChoose))
                .collect(Collectors.toList())
                .get(0);
    }
}
