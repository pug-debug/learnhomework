package com.epam.learn.pasteBinTests;

import com.epam.learn.constants.PasteBinXpaths;
import com.epam.learn.constants.Timeouts;
import com.epam.learn.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class PasteBinHomePage extends BasePage {
    private static final String HOME_PAGE = "https://pastebin.com";

    @FindBy(id = "postform-text")
    private WebElement fieldToPasteText;

    @FindBy(id = "select2-postform-format-container")
    private WebElement syntaxHighlighting;

    @FindBy(xpath = "//span[@class='select2-selection__rendered' and @id='select2-postform-expiration-container']")
    private WebElement pasteExpirationField;

    @FindBy(xpath = "//input[@id='postform-name' and @class='form-control']")
    private WebElement fieldForPasteName;

    @FindBy(xpath = "//*[@class='btn -big']")
    private WebElement createNewPasteButton;

    @FindBy(xpath = "//div[@class='content__title -paste']")
    private WebElement optionalPasteSettings;

    public PasteBinHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public PasteBinHomePage openPage() {
        driver.get(HOME_PAGE);
        new WebDriverWait(driver, Duration.ofSeconds(Timeouts.GENERAL_TIME_OUT_SECONDS))
                .until(ExpectedConditions.visibilityOfAllElements(fieldToPasteText, fieldForPasteName,
                        createNewPasteButton, pasteExpirationField));
        return this;
    }

    public ResultPasteBinPage createNewPasteWithoutHighlighting(String newPasteText, String pasteExpirationOption,
                                                                String pasteNameText) {
        fieldToPasteText.sendKeys(newPasteText);
        scrollToElement(optionalPasteSettings);
        pasteExpirationField.click();
        List<WebElement> expirationOptions = driver.findElements(By.xpath(PasteBinXpaths.PASTE_EXPIRATION_DROPDOWN));
        WebElement optionTime = getNecessaryOptionFromDropbox(expirationOptions, pasteExpirationOption);
        optionTime.click();
        fieldForPasteName.sendKeys(pasteNameText);
        createNewPasteButton.click();
        return new ResultPasteBinPage(driver);
    }

    public ResultPasteBinPage createNewPasteWithHighlighting(String newPasteText, String highlighting,
                                                             String pasteExpirationOption, String pasteNameText) {
        fieldToPasteText.sendKeys(newPasteText);
        scrollToElement(optionalPasteSettings);
        syntaxHighlighting.click();
        List<WebElement> syntaxHighlightingOptions = driver.findElements(By.xpath(PasteBinXpaths.SYNTAX_HIGHLIGHTING_DROPDOWN));
        WebElement highlightingOption = getNecessaryOptionFromDropbox(syntaxHighlightingOptions, highlighting);
        highlightingOption.click();
        pasteExpirationField.click();
        List<WebElement> expirationOptions = driver.findElements(By.xpath(PasteBinXpaths.PASTE_EXPIRATION_DROPDOWN));
        WebElement optionTime = getNecessaryOptionFromDropbox(expirationOptions, pasteExpirationOption);
        optionTime.click();
        fieldForPasteName.sendKeys(pasteNameText);
        createNewPasteButton.click();
        return new ResultPasteBinPage(driver);
    }
}