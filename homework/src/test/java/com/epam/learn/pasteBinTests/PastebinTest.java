package com.epam.learn.pasteBinTests;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PastebinTest {
    private final String pasteText = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force";
    private final String highlighting = "Bash";
    private final String timeExpiration = "10 Minutes";
    private final String pasteName = "how to gain dominance among developers";

    private WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void browseSetup() {
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test(description = "I Can Win")
    public void createNewPasteOnPastebinComTest() {
        ResultPasteBinPage resultPasteBinPage = new PasteBinHomePage(driver)
                .openPage()
                .createNewPasteWithoutHighlighting("Hello from WebDriver", "10 Minutes",
                        "helloweb");
    }

    @Test(description = "Bring It On")
    public void createNewPasteWithHighlightingOnPastebinCom() {

        ResultPasteBinPage resultPasteBinPage = new PasteBinHomePage(driver)
                .openPage()
                .createNewPasteWithHighlighting(pasteText, highlighting, timeExpiration, pasteName);

        Assert.assertTrue(checkResultPageElements(resultPasteBinPage));
    }

    private boolean checkResultPageElements(ResultPasteBinPage resultPage) {
        return pasteName.equals(resultPage.getPasteName()) && pasteText.equals(resultPage.getResultText())
                && highlighting.equals(resultPage.getWayOfHighlighting());
    }

    @AfterMethod(alwaysRun = true)
    public void closeBrowser() {
        driver.quit();
        driver = null;
    }
}