package com.epam.learn.pasteBinTests;

import com.epam.learn.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ResultPasteBinPage extends BasePage {

    @FindBy(xpath = "//textarea[@class='textarea']")
    private WebElement fieldWithResultText;

    @FindBy(xpath = "//div[@class='info-top']/h1")
    private WebElement pasteName;

    @FindBy(xpath = "//div[@class='left']/a")
    private WebElement wayOfHighlighting;

    public ResultPasteBinPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, Duration.ofSeconds(5))
                .until(ExpectedConditions.visibilityOfAllElements(pasteName, fieldWithResultText, wayOfHighlighting));
    }

    public String getResultText() {
        return fieldWithResultText.getText();
    }

    public String getWayOfHighlighting() {
        return wayOfHighlighting.getText();
    }

    public String getPasteName() {
        return pasteName.getText();
    }
}
